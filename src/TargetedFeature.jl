using CSV 
using DataFrames
using Statistics
using ProgressBars
# include("PeakFind.jl")
# include("FeatureDetection.jl")

##################################

##################################
# feature extraction xic 

function featureextractPeak(chrom,mz,mz_tol,min_int,min_pt)
    
    feat_f = zeros(1,12)
    mz_v = chrom["MS1"]["Mz_values"]
    mz_i = chrom["MS1"]["Mz_intensity"];
    

    for i = 1:length(mz)
        mz_temp = mz[i]
        #println(i)
        xic, mz_m, xic_sum = xic_extract(mz_v,mz_i,mz_temp,mz_tol)
        if length(xic) > 0 && maximum(xic) >= min_int
            xic_s = sma(xic, 3)
            peaks = findpeaks(xic_s[:], collect(1:length(xic)), min_height=float(min_int) , min_dist= 2 * min_pt)
            if length(peaks) == 0
                continue
            end 
            out1 = peak_check(xic,min_pt,peaks)
            out = out1[out1[:,1] .>0,:]
            feat = zeros(size(out,1),12)
            #println(i)
            for j = 1:size(out,1)
                if out[j,1] == 0
                    continue
                end 
                feat[j,1] = i
                feat[j,2] = out[j,1]
                feat[j,3] = out[j,3] - out[j,2]
                feat[j,4] = chrom["MS1"]["Rt"][Int(out[j,1])]
                feat[j,5] = chrom["MS1"]["Rt"][Int(out[j,2])]
                feat[j,6] = chrom["MS1"]["Rt"][Int(out[j,3])]
                feat[j,7] = feat[j,6] - feat[j,5]
                feat[j,8] = mean(mz_m)
                feat[j,9] = minimum(mz_m)
                feat[j,10] = maximum(mz_m)
                feat[j,11] = round(xic_s[peaks[j]])
                feat[j,12] = sum(xic_s[Int(out[j,2]):Int(out[j,3])])
            end 

            feat_f = vcat(feat_f,feat[feat[:,1] .>0,:])


        end 



    end 
    
    return feat_f[2:end,:]
end

##################################
# XIC check

function peak_check(xic_s,min_pt,peaks)
     
    cm_2 = median(cummean(xic_s[xic_s .>0]))
    out = zeros(length(peaks),3)
    for i = 1:length(peaks)
        if xic_s[peaks[i]] < 1.5* cm_2
            continue
        end
        #println(i) 

        if isnothing(findlast(x -> x <= cm_2, xic_s[1:peaks[i]])) || isnothing(findfirst(x -> x <= cm_2, xic_s[peaks[i]:end]))
            continue
        end 
        lb = findlast(x -> x <= cm_2, xic_s[1:peaks[i]])
        ub = peaks[i] + findfirst(x -> x <= cm_2, xic_s[peaks[i]:end])
       
        pt = ub - lb
        if pt >= min_pt && pt <= 5*min_pt
            out[i,1] = peaks[i]
            out[i,2] = lb
            out[i,3] = ub
        elseif pt > 3*min_pt
            out[i,:] .= 0
        else
            out[i,:] .= 0
        end 


    end
    
    return out 
    
end

##################################
# feature extraction xic 

function featureextractXIC(chrom,mz,mz_tol,min_int,min_pt)
    feat = zeros(length(mz),11)
    mz_v = chrom["MS1"]["Mz_values"]
    mz_i = chrom["MS1"]["Mz_intensity"];
    

    for i = 1:length(mz)
        mz_temp = mz[i]
        #println(i)
        xic, mz_m, xic_s = xic_extract(mz_v,mz_i,mz_temp,mz_tol)
        if length(xic) > 0 && maximum(xic) >= min_int
            out = xic_check(xic,min_pt)
            if length(out) == 0
                continue
            end 
            #println(i)
            feat[i,1] = out[1]
            feat[i,2] = out[3] - out[2]
            feat[i,3] = chrom["MS1"]["Rt"][out[1]]
            feat[i,4] = chrom["MS1"]["Rt"][out[2]]
            feat[i,5] = chrom["MS1"]["Rt"][out[3]]
            feat[i,6] = feat[i,5] - feat[i,4]
            feat[i,7] = mean(mz_m)
            feat[i,8] = minimum(mz_m)
            feat[i,9] = maximum(mz_m)
            feat[i,10] = maximum(xic)
            feat[i,11] = sum(xic_s[out[2]:out[3]])


        end 



    end 
    return feat
end


# plot(xic)

##################################
# XIC check

function xic_check(xic,min_pt)
    loc = argmax(xic)
    if loc[1] == 1 || loc[1] == length(xic)
        return out = []
    end  
    cm_2 = median(cummean(xic[xic .>0]))
    lb = findlast(x -> x <= cm_2, xic[1:loc[1]])
    ub = loc[1] + findfirst(x -> x <= cm_2, xic[loc[1]:end])
    if isempty(lb) == 1 || isempty(ub) == 1
        return out = []
    end 
    pt = ub - lb
    if pt >= min_pt
        scan_n = loc[1]
    else
        return out = []
    end 

    out = [scan_n,lb,ub]
    
    return out 
    
end


##################################
# XIC extract

function xic_extract(mz_v,mz_i,mz_temp,mz_tol)

    md = abs.(mz_v .- mz_temp);
    mz_i1 = deepcopy(mz_i)
    mz_i1[md .>= mz_tol] .=0



    xic = maximum(mz_i1,dims=2)
    xic_s = sum(mz_i1,dims=2)
    return xic, mz_v[mz_i1 .>0], xic_s

end




##################################
# Targeted feature detection

function targetfeature(chrom,mz,mz_tol,min_int,min_pt ::Int64 = 3, m::String = "Peak")
    rep = []
    if m == "Peak" || m == "peak"
        rep = featureextractPeak(chrom,mz,mz_tol,min_int,min_pt)
        table = DataFrame(rep,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
            :MeasMass,:MinMass,:MaxMass,:Int,:Area])

    elseif m == "XIC" || m == "xic"
        rep = featureextractXIC(chrom,mz,mz_tol,min_int,min_pt)
        rep_table = hcat(collect(1:length(mz)),rep)

        table = DataFrame(rep_table,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
            :MeasMass,:MinMass,:MaxMass,:Int,:Area])
    else 
        println("This file will be processed with default settings!")
        rep = featureextractPeak(chrom,mz,mz_tol,min_int,min_pt)
        table = DataFrame(rep,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
            :MeasMass,:MinMass,:MaxMass,:Int,:Area])

    end 
    if length(rep) == 0
        println("Something went wrong!")
        return []

    end 

    
    
    return table
    
end

"""
##################################
# Targeted feature detection batch 

function targetfeature_batch(path2files,mz_thresh,int_thresh,mz,mz_tol,min_int,min_pt ::Int64 = 3, m::String = "Peak")
    nn = readdir(path2files)

    for i in ProgressBar(1:size(nn,1))
        f = [nn[i]]
        ft = split(nn[i],".")
        if length(ft[1]) > 0 && ft[end] == "mzXML"
            
            chrom = import_files(path2files,f,mz_thresh,int_thresh)
            table = targetfeature(chrom,mz,mz_tol,min_int,min_pt,m)
            outf = joinpath(path2files,ft[end-1] * ".csv")
            if size(table,1) >0
                CSV.write(outf,table)
            end 
        end 


    end 

    
end







##################################
# testing area
# 

using MS_Import
using Plots 

# File chrom
pathin = "/Users/saersamanipour/Desktop/dev/pkgs/SAFD/test"
filenames = ["test_chrom.mzXML"]

mz_thresh=[0,550]
int_thresh=300

ch=import_files(pathin,filenames,mz_thresh,int_thresh)
chrom = deepcopy(ch)

# import mzs 

mz_t = DataFrame(CSV.File("/Volumes/SAER HD/Data/Temp_files/WCM2022/NAs.csv"))
#mz = mz_t[!,"Mass-H"]
mz = 199.1628

# Param 

m = "XIC"
mz_tol = 0.01
min_int = 500
min_pt = 3

path2files = "/Volumes/SAER HD/Data/Temp_files/WCM2022/mzXML/Neg"

"""
