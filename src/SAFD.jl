__precompile__()
module SAFD

using LsqFit
using Dierckx
using Statistics
using DataFrames
using CSV
using DSP
using BenchmarkTools
using FileIO

#using ScikitLearn
#using Conda

using ProgressBars
using Dates
using Downloads
using EvoTrees

"""
if Sys.iswindows()
    #move miniconda installer in case it is in the Conda/3/ folder
    pathConda = pathof(Conda)
    if contains(pathConda, "\\")
        pathConda = split(pathConda, "\\packages")[1]
    end
    pathConda = joinpath(pathConda, "conda", "3","installer.exe")



    if isfile(pathConda)
        mv(pathConda, pathConda[1:end-15]*pathConda[end-12:end], force = true)
    else
        Downloads.download("https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe",pathConda)
    end
end

using PyCall 

Conda.add("joblib")

const jl = PyNULL()

function __init__()
    copy!(jl,pyimport("joblib"))

end


""" 



include("Centroid.jl")
include("Profile.jl")
include("FeatureDetection.jl")
include("FeatureDetectionGC.jl")
include("FeatureAlign.jl")
include("PeakFind.jl")
include("TargetedFeature.jl")



export safd, safd_s3D, safd_s3d_cent, centroid, dmz_pred_batch, dmz_pred, feature_align, targetfeature, SAFD_stitch, lilo_s_safd, nozero, safd_multicore, safd_gc_mc, safd_s3D_GC, SAFD_GC_stitch, split_setup


end # End of the module

#################################################################
#
