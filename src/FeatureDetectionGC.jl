
using LsqFit
using Dierckx
using Statistics
using DataFrames
using CSV
using DSP
using BenchmarkTools
#using ProgressMeter
using ProgressBars
using Dates
using LinearAlgebra
using SparseArrays, SuiteSparse
using Hwloc
#################################################################################



function WhittakerSmooth(x::Vector,lambda,differences)
   
    L=length(x)
    w = ones(L)
    E=sparse(I,L,L)
    for i = 1:differences
        E = E[2:end,:] - E[1:end-1,:] 
    end

    W=sparse(1:L,1:L,w)
    A = W + lambda * E' * E
    B = W * x 
    background = A \ B
    return(background)
end



function arPLS_baseline_v0(y_t, lambda, ratio,itermax)
    baseline=y_t
    signal_length = length(y_t)
    difference_matrix=sparse(I,signal_length,signal_length)

    for i = 1:1
        difference_matrix = difference_matrix[2:end,:] - difference_matrix[1:end-1,:] 
    end

    minimization_matrix = (lambda * transpose(difference_matrix)) * difference_matrix
    penalty_vector = ones(signal_length)

    for count = 1:itermax
        penalty_matrix = Diagonal(penalty_vector)
        C = cholesky(penalty_matrix + minimization_matrix)
        baseline = C \ (C' \ (penalty_vector .* y_t))
        d = y_t - baseline
        dn = d[d .< 0]
        m = mean(dn)
        s = std(dn)
        pt=ones(length(d))
        d= d .- ((2*s-m))
        d= d .* 2
        d= d ./ s
        d= exp.(d)
        d= d .+ 1
        penalty_vector_temp = pt ./ d
      
        if norm(penalty_vector - penalty_vector_temp) / norm(penalty_vector) < ratio
            
            return baseline
            break
        end
        penalty_vector = penalty_vector_temp
    end
    return baseline
end

function max_sig_finder_GC(mz_vals,mz_int,max_t_peak_w,min_int,bm)
    mind = argmax(mz_int .* bm)
    #mind = CartesianIndex(2161, 8864)
    m = mz_int[mind]
    #m=mz_int[814,79]
    
    if m < min_int
        t_y_r = Matrix{Float32}(undef, 0, 0)
        t_x_r = Matrix{Float32}(undef, 0, 0)
        bounds = Vector{Int64}(undef, 0)
        mind=CartesianIndex(1,2)
        ms_maxim = Float32(0.0)
        return (t_y_r,t_x_r,bounds,mind,ms_maxim)
    end
    pos1 = mind[1]-max_t_peak_w
    
    pos3 = mind[1]+max_t_peak_w 
   pos3 = min(pos3, size(mz_vals, 1))
    bounds_pos2 = min(mind[1], max_t_peak_w + 1)
        #mind=CartesianIndex(205,13824)
    pos1 = (pos1 >= 1) * pos1 + (pos1 <= 1) * 1

    t_y_r=mz_int[pos1:pos3, :]
    t_x_r=mz_vals[pos1:pos3, :]
    bm1_r=bm[pos1:pos3,:]
    bounds=[pos1,bounds_pos2,pos3]
    ms_maxim=Float32(mz_vals[mind])
  
    return (t_y_r,t_x_r,bounds,mind,ms_maxim,bm1_r)

end




####################################################################
#Simple moving average

function sma2_GC(y, n)

    vals = zeros(size(y,1) ,1)
    for i in 1:size(vals,1)-(n-1)
        vals[i+1] = mean(y[i:i+(n-1)])
    end
    for i = size(vals,1)-(n-2):size(vals,1)-1
        vals[i]=vals[size(vals,1)-(n-2)] 
    end
    vals[1]=y[1]
    vals[end]=y[end]
    return vals
end


function sma_GC(y, n)

    vals = zeros(size(y,1) ,1)
    for i in 1:size(vals,1)-(n-1)
        vals[i+1] = mean(y[i:i+(n-1)])
    end

    vals[1]=y[1]
    vals[end]=y[end]
    return vals
end

####################################################################
#This function produced the mass vectors for limited to the peak
#
##


function res2mass_window_GC(mz_vect,int_vect,res,min_ms_w,ms_maxim)
    tv1 = abs.(mz_vect .- ms_maxim)
    tv2 = length(tv1[tv1 .<= min_ms_w])

    #plot(tv1[tv1 .<= min_ms_w],filter(x->x <= min_ms_w,tv1))


    if  tv2 > 0

        # plot(int_vect)
        dms=2*(ms_maxim/res)
        if dms < min_ms_w
            dms = min_ms_w
        end
        x_t = mz_vect[tv1 .<= dms]
        y_t = int_vect[tv1 .<= dms]

        #plot(mz_vect[tv1 .<= dms/2],int_vect[tv1 .<= dms/2])

    else

        x_t=[]
        y_t=[]


    end
    return (x_t,y_t)


end


######################################################################

function simple_gauss_GC(x2fit,y2fit,g_numb,r_thresh,min_ms_w)
    if g_numb == 1
        @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))

        p=zeros(3)
        p[1]=maximum(y2fit)
        p[2]=x2fit[argmax(y2fit)]
        p[3]=(maximum(x2fit)-minimum(x2fit))
        lbp=[0,x2fit[argmax(y2fit)]-min_ms_w,0]
        ubp=[Inf,x2fit[argmax(y2fit)]+min_ms_w,Inf]
        fit1=[]
        r2=0

        try
            fit1 = curve_fit(model ,x2fit, y2fit, p)
            r2=1 - var(fit1.resid) / var(y2fit)
            #println(r2)
        catch
            fit1=[]
            r2=0

        end
        #y_r=model(x_t,fit1.param)
        if r2 < r_thresh
            try
                fit1 = curve_fit(model ,x2fit, y2fit, p,lower=lbp, upper=ubp)
                r2=1 - var(fit1.resid) / var(y2fit)

            catch
                fit1=[]
                r2=0

            end

        end

        return (r2,fit1)
    elseif g_numb == 2
        @.model1(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))+
        (p[4]/(p[6]*sqrt(pi*2))) * exp(-(x-p[5])^2 / (2*p[6]^2))

        p=zeros(6)
        p[1]=maximum(y2fit)
        p[2]=x2fit[argmax(y2fit)]
        p[3]=(maximum(x2fit)-minimum(x2fit))
        p[4]=maximum(y2fit)
        p[5]=x2fit[argmax(y2fit)]
        p[6]=(maximum(x2fit)-minimum(x2fit))
        lbp=[0,x2fit[argmax(y2fit)]-min_ms_w,0,
        0,x2fit[argmax(y2fit)]-min_ms_w,0]
        ubp=[Inf,x2fit[argmax(y2fit)]+min_ms_w,Inf,
        Inf,x2fit[argmax(y2fit)]+min_ms_w,Inf]
        fit1=[]
        r2=0

        try
            fit1 = curve_fit(model1, x2fit, y2fit, p)
            r2=1 - var(fit1.resid) / var(y2fit)

        catch
            fit1=[]
            r2=0
        end

        if r2 < r_thresh

            try
                fit1 = curve_fit(model1 ,x2fit, y2fit, p,lower=lbp, upper=ubp)
                r2=1 - var(fit1.resid) / var(y2fit)
            catch
                fit1=[]
                r2=0

            end
        end


        return (r2,fit1)


    else
        println("The number of Gaussians to fit is limited to two.")
        r2=0
        fit=[]
        return (r2,fit)

    end

end




######################################################################
#


function signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)

    if size(fit1.param,1) == 3
        @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2));
        y_r=model(x_t,fit1.param)
        #y_r1=model(x2fit,fit.param)
        x_max=x_t[argmax(y_r)]
        s_sig=length(y_r[y_r.>min_int/2])

    elseif size(fit1.param,1) == 6
        @.model1(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))+
        (p[4]/(p[6]*sqrt(pi*2))) * exp(-(x-p[5])^2 / (2*p[6]^2))
        y_r=model1(x_t,fit1.param)
        x_max=x_t[argmax(y_r)]
        s_sig=length(y_r[y_r.>min_int/2])

    end
    x_rep = x_t[y_r.> 0]
    y_rep = y_t[y_r.> 0]
    if length(y_r[y_r .> min_int/2]) == length(y_t[y_t .> min_int/2])
        y_t[y_t .> min_int/2] = (min_int/2)*ones(length(y_t[y_r .> min_int/2]))
    else
        y_t[y_t .> min_int/2]=(min_int/2)*ones(length(y_t[y_t .> min_int/2]))
    end


    return (x_rep,y_rep,y_t,x_max,s_sig)


end




#######################################################################


function peak_detect_n_interp_GC(mz_vect,int_vect,res,min_ms_w,r_thresh,ms_maxim,min_int)

    x_t,y_t = res2mass_window_GC(mz_vect,int_vect,res,min_ms_w,ms_maxim)


    
    

        if length(x_t)>0 && maximum(y_t)> min_int/2 && length(y_t[ y_t .> min_int])>=2 





            x2fit, y2fit, res_m = isolate_sig_n_interp_GC(x_t,y_t)


            if length(x2fit)>3 #&& maximum(y_t)/median(y_t)>=S2N
                g_numb=1
                r2,fit1=simple_gauss_GC(x2fit,y2fit,g_numb,r_thresh,min_ms_w)

                if r2 <= r_thresh

                    r2,fit1=simple_gauss_GC(x2fit,sma_GC(y2fit,3)[:],g_numb,r_thresh,min_ms_w)

                end

                if r2 <= r_thresh && length(x2fit)>5
                    g_numb=2
                    r2,fit1=simple_gauss_GC(x2fit,y2fit,g_numb,r_thresh,min_ms_w)
                end


                if r2 >= r_thresh
                    x_rep,y_rep,y,x_max,s_sig = signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)
                else

                    y_t=(min_int/2)*ones(length(y_t))
                    y=y_t
                    s_sig=maximum(y2fit)
                    x_rep=x2fit
                    y_rep=y2fit
                    x_max=ms_maxim

                end
            elseif length(x2fit)>0 && length(x2fit) <= 3
                y_t=(min_int/2)*ones(length(y_t))
                y=y_t
                s_sig=maximum(y2fit)
                x_rep=x2fit
                y_rep=y2fit
                x_max=ms_maxim

            else
                y_t=(min_int/2)*ones(length(y_t))
                y=y_t
                s_sig=0
                x_rep=[]
                y_rep=[]
                x_max=[]
                res_m = []
                
            end


        else
            y_t=(min_int/2)*ones(length(y_t))
            y=y_t
            s_sig=0
            x_rep=[]
            y_rep=[]
            x_max=[]
            res_m = []
           

        end

    return (x_rep,y_rep,y,x_t,x_max,s_sig,res_m)


end




#######################################################################
## movine window mean

function moving_w_cummean_GC(XIC,min_int) # this function is to find the moving window mean of the XIC to determine if the XIC needs smoothing

    xcm=zeros(10)
    xcm[1] = mean(XIC[1:(round(Int,length(XIC)/10))])
    for i = 2:9
        xcm[i] = mean(XIC[(i-1)*(round(Int,length(XIC)/10)):i*(round(Int,length(XIC)/10))])
    end
    xcm[10]= mean(XIC[9*(round(Int,length(XIC)/10)):end])
    xmax=maximum(xcm)
    xmin=minimum(xcm)
    if xmax <= min_int && xmin <= min_int
        xrat = 0
    else
        xrat = 1
    end
    return xrat
    
end






######################################################################
## cummulative mean

# function cummean(XIC)
#     cm = zeros(size(XIC))
#     cm[1] = XIC[1]

#     for i=2:length(XIC)
#         cm[i] = mean(XIC[1:i])
#     end
#     return cm

# end


######################################################################
## Pseudo feature detection
function feature_detect_s3D_GC(Rt,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,
    r_thresh,S2N,min_peak_w_s,bm)
    lambda=10000 #high lambda for baseline calculation
    ratio = 0.000001
    differences = 2
    Lam=100 #low lambda for smoothing
    itermax=1000

    t_y_r,t_x_r,bounds,mind,ms_maxim,bm1_r = max_sig_finder_GC(mz_vals,mz_int,max_t_peak_w,min_int,bm) #finds the maximum intensity and returns area of detection


    if size(t_y_r,1)>0 
        mz_vect=t_x_r[bounds[2],:] #finds the mass vector for the maximum intensity point found
        int_vect=t_y_r[bounds[2],:] #finds the intensity vector for the maximum intensity point found
        bm_vect=bm1_r[bounds[2],:]
        mz_vect=mz_vect[bm_vect .> 0]
        int_vect=int_vect[bm_vect .> 0]

        x_rep,y_rep,y,x_t,x_max,s_sig,res_m = peak_detect_n_interp_GC(mz_vect,int_vect,res,min_ms_w,r_thresh,ms_maxim,min_int)
        x_t_test=deepcopy(x_t)
        x_t_test= deleteat!(x_t_test, x_t_test.<=0.1)
        if length(x_t_test) <= 1
            
            del_me = (t_x_r .>= (minimum(x_t) - (min_ms_w / 10))) .& (t_x_r .<= (maximum(x_t) + (min_ms_w / 10)))

            bm1_r[del_me .==1] .= 0
            bm[bounds[1]:bounds[3],:]=bm1_r
            scan_num=mind[1]
            p_w_s=[]
            ret_t=[]
            t_i=[]
            t_e=[]
            p_w_t=[]
            mass_meas=[]
            min_mass=[]
            max_mass=[]
            feat_int=[]
            feat_purity=[]
            sig_r = 2*min_int
    
            med_res=[]


            return(sig_r,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
            feat_purity,med_res,bm) 
        end
        
        del_me = (mz_vals .>= floor(minimum(x_t), digits=5)) .& (mz_vals .<= ceil(maximum(x_t), digits=5))


        t_y_r_te = deepcopy(mz_int) #copies the mz_int matrix

        t_y_r_te[del_me .< 1] .= 0 #removes mz values outside the tolerance
        XICe2 = maximum(t_y_r_te,dims =2) #creates the XI
        XICe2 = vec(XICe2)
        t_y_r_te=t_y_r_te.*bm #removes mz values that have been checked before
        XICe = maximum(t_y_r_te,dims =2) #creates the XI
        XICe = vec(XICe)
        xrat=moving_w_cummean_GC(XICe,min_int) #checks the moving window mean of the XIC to determine if the XIC needs smoothing
        xmind=length(XICe)

        if xrat == 1 #if the XIC needs smoothing
            z2=sma2_GC(XICe2,300)
            XICw=XICe.-z2
            if maximum(XICw) <= min_int # case when xic is nothing on SMA filter

                bm[del_me .==1] .= 0


                scan_num=mind[1]
                p_w_s=[]
                ret_t=[]
                t_i=[]
                t_e=[]
                p_w_t=[]
                mass_meas=[]
                min_mass=[]
                max_mass=[]
                feat_int=[]
                feat_purity=[]
                sig_r = 2*min_int
        
                med_res=[]


                return(sig_r,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
                feat_purity,med_res,bm) 
            else # case when xic is something on SMA filter



                z=arPLS_baseline_v0(XICe2, lambda, ratio,itermax)
                XICw2=WhittakerSmooth(XICe,Lam,differences)

                XICw=XICw2.-z

                if maximum(XICw) <= min_int # case when smoothed baseline corrected xic is nothing

                    bm[del_me .==1] .= 0


                    scan_num=mind[1]
                    p_w_s=[]
                    ret_t=[]
                    t_i=[]
                    t_e=[]
                    p_w_t=[]
                    mass_meas=[]
                    min_mass=[]
                    max_mass=[]
                    feat_int=[]
                    feat_purity=[]
                    sig_r = 2*min_int
            
                    med_res=[]


                    return(sig_r,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
                    feat_purity,med_res,bm)
                else # case when smoothed baseline corrected XIC still has peaks
                    halfit=floor(Int,(length(z)/2))
                    zpoint=argmax(z[halfit:end])
                    zarg=maximum(z[zpoint:end])
                    xmax=maximum(XICe[zpoint:end])
                    

                    if zarg > xmax  #this is a filter for overly messy XICS
                        xmind = zpoint

                        bm1=bm[xmind:end,:]
                        del_me2=del_me[xmind:end,:]
                        bm1[del_me2 .==1] .= 0
                        bm[xmind:end,:] = bm1
                        if mind[1] >= xmind
                            scan_num=mind[1]
                            p_w_s=[]
                            ret_t=[]
                            t_i=[]
                            t_e=[]
                            p_w_t=[]
                            mass_meas=[]
                            min_mass=[]
                            max_mass=[]
                            feat_int=[]
                            feat_purity=[]
                            sig_r = 2*min_int
                    
                            med_res=[]
                    
                            return(sig_r,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
                            feat_purity,med_res,bm)   
                        end  

                    elseif maximum(XICw[halfit:end]) <= min_int # this filters out XICs where there's nothing of interest after halfway

                        xmind = halfit

                        bm1=bm[xmind:end,:]
                        del_me2=del_me[xmind:end,:]
                        bm1[del_me2 .==1] .= 0
                        bm[xmind:end,:] = bm1
                        if mind[1] >= xmind + max_t_peak_w 
                            scan_num=mind[1]
                            p_w_s=[]
                            ret_t=[]
                            t_i=[]
                            t_e=[]
                            p_w_t=[]
                            mass_meas=[]
                            min_mass=[]
                            max_mass=[]
                            feat_int=[]
                            feat_purity=[]
                            sig_r = 2*min_int
                    
                            med_res=[]
                    
                            return(sig_r,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
                            feat_purity,med_res,bm)   
                        end

                    elseif isnothing(findlast(x -> x > min_int,XICw) )==0 # finds the last peak of interest in the XIC
                        xmind = findlast(x -> x >= min_int,XICw)
                        if xmind + max_t_peak_w >= length(XICe)
                            xmind = length(XICe)
                        else
                            xmind = xmind + max_t_peak_w

                            bm1=bm[xmind:end,:]
                            del_me2=del_me[xmind:end,:]
                            bm1[del_me2 .==1] .= 0
                            bm[xmind:end,:] = bm1
                            bm1_r=bm[bounds[1]:bounds[3],:]
                        end
                       



                    end
                
                end    
            end    
        end


        del_m = (t_x_r .>= floor(minimum(x_t), digits=5)) .& (t_x_r .<= ceil(maximum(x_t), digits=5))

        t_y_r_t = deepcopy(t_y_r)
        t_y_r_tcm = deepcopy(t_y_r)
        t_y_r_tcm[del_m .<1] .= 0
        XICcm = maximum(t_y_r_t,dims =2)

        t_y_r_t[del_m .<1] .= 0
        t_y_r_t = t_y_r_t .* bm1_r # this is because it kept finding the same peak over and over. is there a better way!?
        XIC = maximum(t_y_r_t,dims =2)
        XICcm=vec(XICcm)
        XIC=vec(XIC)
        argxic=argmax(XIC)
        if xrat == 1
            XICcm=XICe2[bounds[1]:bounds[3]]
            XICcm=WhittakerSmooth(XICcm,Lam,differences)
            zbase=z2[bounds[1]:bounds[3]]
            XIC2=WhittakerSmooth(XIC,Lam,differences)
            sig = XIC2 .- zbase
            comppt=zbase[argxic]
        else

            if median(XICe2)>= 3 #checks the unfiltered XIC to see if it's median is greater than 3

                zbase=arPLS_baseline_v0(XICe2, lambda, ratio,itermax)
                XIC2=WhittakerSmooth(XICe,Lam,differences)
                zbase=zbase[bounds[1]:bounds[3]]
                XIC2=XIC2[bounds[1]:bounds[3]]
                sig = XIC2 .- zbase

                comppt=zbase[argxic]

            else

                sig=XIC.-3
                comppt=3
                compmean=3
            end
        end
 
 

        if isnothing(findlast(x -> x <=0, sig[1:bounds[2]]))==0 && isnothing( findfirst(x -> x <=0, sig[bounds[2]:end]))==0

            lb = findlast(x -> x <=0, sig[1:bounds[2]])
            ub = bounds[2] + findfirst(x -> x <=0, sig[bounds[2]:end]) - 1
        elseif isnothing(findlast(x -> x <=0, sig[1:bounds[2]]))==0 && isnothing( findfirst(x -> x <=0, sig[bounds[2]:end]))> 0
            lb = findlast(x -> x <=0, sig[1:bounds[2]])
            ub = size(t_y_r,1)
        elseif isnothing(findlast(x -> x <=0, sig[1:bounds[2]]))> 0 && isnothing( findfirst(x -> x <=0, sig[bounds[2]:end]))==0
            ub = bounds[2] + findfirst(x -> x <=0, sig[bounds[2]:end]) - 1
            lb = 1
        else
            lb = 1
            ub = size(t_y_r,1)

        end
        if  lb >= ub

            lb=bounds[2] - min_peak_w_s
            ub=bounds[2] + min_peak_w_s
            if lb <= 1
                lb=1
            end
            if ub >= size(t_y_r,1)
                ub=size(t_y_r,1)
            end
            del_m2 = del_m[lb:ub,:]
            del_m3 = deepcopy(del_m2)

    
            bm1_r = bm[bounds[1]:bounds[3],:]
            bm2=bm1_r[lb:ub,:]
            bm2[del_m3 .==1] .= 0
            bm1_r[lb:ub,:]=bm2
            bm[bounds[1]:bounds[3],:]=bm1_r#double check this liam
            scan_num=mind[1]
            p_w_s=[]
            ret_t=[]
            t_i=[]
            t_e=[]
            p_w_t=[]
            mass_meas=[]
            min_mass=[]
            max_mass=[]
            feat_int=[]
            feat_purity=[]
            sig_r = 2*min_int
    
            med_res=[]
            return(sig_r,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
            feat_purity,med_res,bm)

        end

        t_y_r_t2 = t_y_r[lb:ub,:]
        del_m2 = del_m[lb:ub,:]
        del_m3 = deepcopy(del_m2)
        feature = t_y_r_t2[del_m2 .==1]

        bm1_r = bm[bounds[1]:bounds[3],:]
        bm2=bm1_r[lb:ub,:]
        bm2[del_m3 .==1] .= 0
        bm1_r[lb:ub,:]=bm2
        bm[bounds[1]:bounds[3],:]=bm1_r#double check this liam

        if s_sig >0 && maximum(XIC)-comppt >= min_int && length(bm2) >= min_peak_w_s #&& maximum(XIC)/abs(comppt)>=S2N


            y=XIC[lb:ub]
            y = WhittakerSmooth(y,Lam,differences)



            g_numb,r2=simple_gauss_t_GC(y,r_thresh)


            if r2>=r_thresh

                p_w_s=length(y)
                t_i = Rt[bounds[1] + lb]
                t_e = Rt[bounds[1] + ub - 1]
                scan_num=mind[1]
                ret_t=Rt[mind[1]]
                p_w_t=round(t_e - t_i,digits=2)
                mass_meas = round(mean(x_t),digits=4)
                min_mass = round(minimum(x_t),digits=4)
                max_mass = round(maximum(x_t),digits=4)
                feat_int = round(maximum(XIC[lb:ub,:]))
                feat_purity = g_numb
                sig_r = sum(feature)
                med_res = res_m
                r2_end = r2
                positive=1


            else
                scan_num=mind[1]
                p_w_s=[]
                ret_t=[]
                t_i=[]
                t_e=[]
                p_w_t=[]
                mass_meas=[]
                min_mass=[]
                max_mass=[]
                feat_int=[]
                feat_purity=[]
                sig_r = 2*min_int
        
                med_res=[]
                positive=0



            end
        else
            scan_num=mind[1]
            p_w_s=[]
            ret_t=[]
            t_i=[]
            t_e=[]
            p_w_t=[]
            mass_meas=[]
            min_mass=[]
            max_mass=[]
            feat_int=[]
            feat_purity=[]
            sig_r = 2*min_int
    
            med_res=[]



        end

    else
        scan_num=mind[1]
        p_w_s=[]
        ret_t=[]
        t_i=[]
        t_e=[]
        p_w_t=[]
        mass_meas=[]
        min_mass=[]
        max_mass=[]
        feat_int=[]
        feat_purity=[]
        sig_r = 0

        med_res=[]




    end

    return(sig_r,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
    feat_purity,med_res,bm)

end # function




##############################################################################

function isolate_sig_n_interp_GC(x,y) # this function isolates the signal and interpolates it to the maximum


    m_y = maximum(y)
    m_y_d = y .- m_y/4

    x2_out = x[m_y_d .>= 0]
    y2_out = y[m_y_d .>= 0]
    if length(x2_out) > 0
        res_m = round(x[argmax(y)]/(x2_out[end]-x2_out[1]))
    else
        res_m = []
    end

    return(x2_out, y2_out,res_m)

end




##############################################################################
# Simple Gaussian function time domain

function simple_gauss_t_GC(y,r_thresh) # this function fits a simple gaussian to either the mass at time or time vector
    @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))

    x=range(1, stop=length(y))
    p=zeros(3)
    p[1]=maximum(y)
    p[2]=x[argmax(y)]
    p[3]=(maximum(x)-minimum(x))

    fit1=[]
    r2=0
    g_numb=[]

    try # this is to catch errors in the fitting
        fit1 = curve_fit(model ,1:length(y), y, p)
        r2=round(1 - var(fit1.resid) / var(y); digits=2)
        g_numb=1
        #println(r2)
    catch
        fit1=[]
        r2=0
        g_numb=1

    end
    if r2<r_thresh 
        @.model1(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))+
        (p[4]/(p[6]*sqrt(pi*2))) * exp(-(x-p[5])^2 / (2*p[6]^2))

        p=zeros(6)
        p[1]=maximum(y)
        p[2]=x[argmax(y)]
        p[3]=(maximum(x)-minimum(x))
        p[4]=maximum(y)
        p[5]=x[argmax(y)]
        p[6]=(maximum(x)-minimum(x))

        fit1=[]
        r2=0

        # plot(y)
         # plot!(model1(x,fit1.param))
        try
            fit1 = curve_fit(model1 ,1:length(y), y, p)
            r2=round(1 - var(fit1.resid) / var(y); digits=2)
            g_numb=2

        catch
            fit1=[]
            r2=0
            g_numb=2
        end
    end

    return(g_numb,r2)


end # function


######################################################################
## Wrapper function

function safd_s3D_GC(mz_vals::Array{Float32,2},mz_int::Array{Float32,2},Rt::Array{Float32,1},max_numb_iter::Int64,
    max_t_peak_w::Int64,res::Int64,min_ms_w::Float64,r_thresh::Float64,
    min_int::Int64,S2N::Int64,min_peak_w_s::Int64)

    #hz=(size(mz_int,1)/(60*(t_end-t0)))
    rep_table=zeros(max_numb_iter,15); # preallocating the report table
    bm=ones(UInt8,size(mz_int,1),size(mz_int,2)) # preallocating the binary table
    bm[mz_int.<=min_int].=0 # filtering out the low intensity values


    # i=1

    for i in ProgressBar(1:max_numb_iter, printing_delay=0.1)

        try
        sig,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
        feat_purity,med_res,bm = feature_detect_s3D_GC(Rt,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,
        r_thresh,S2N,min_peak_w_s,bm); # this is the main function that does the feature detection




        if length(med_res) > 0 && med_res[1] > 0 && sig > min_int # this is the filter for the feature detection function

            rep=[scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,sig,feat_int,
            feat_purity,med_res[1],i] # this is the output of the feature detection function
            rep_table[i,2:end]=rep # this is the output of the feature detection function put into the report table


        elseif sig>=0 && sig <= min_int # this is the filter for break funcion
            break

        end
    catch
        break
    end
    end






    return (rep_table)



end # function







###########################

function SAFD_GC_stitch(safdv,splits_no,C,pathin,filenames) #this function stitches the safd output together
    for i = 1:splits_no #this ensures there is output for each split
        if safdv[i] == undef
            safdv[i] = zeros(15)
        end
    end
    for j = 2:splits_no #merges into one vector
        safdv[1]=vcat(safdv[1],safdv[j])
    end
    
    safdv=DataFrame(safdv[1], :auto) 
    sort!(safdv,[:x8,:x2]) # sorts it into the order of the output table
    safdv=Matrix(safdv)

    for i = 1:size(safdv,1),j=1:(splits_no-1) #this is the filter for the safd output
        if (safdv[i,8] >(round(C[j],digits=2)-0.5)) && (safdv[i,8] <(round(C[j],digits=2)+0.5)) #checks every single lower mass entry to see if it's in the filter ranges
            scancheck=safdv[i,2]
            for k=(i+1):size(safdv,1)#if it is in the filter range then checks every entry after that point to see if it's in the same filter range
            if (safdv[k,8] >(round(C[j],digits=2)-0.5)) && (safdv[k,8] <(round(C[j],digits=2)+0.5)) 
                scancheck1=safdv[k,2]
                if (scancheck1<(scancheck+5)) && (scancheck1>(scancheck-5))#if it's the same filter range it checks to see if it's in the same scan range roughly
                    safdv[k,:]=safdv[k,:].=0 #if it is, it zeros it
                    #scancheck1=0 # then zeros the scan check 
                else
                end

            else
                scancheck1=0#this just zeros the scan check just in case
            end
            end
        else #if it's not it just zeros out the scan check, I don't know why I did this but it works so don't touch it
            scancheck=0
        end
    end
    #safdv=safdv[vec(mapslices(col -> any(col .!= 0), safdv, dims = 2)), :]#removes the zeros again
    safdv=DataFrame(safdv, [:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes, :iteration_no]) 
    sort!(safdv,[:Rt,:MeasMass])# sorts it into the order I want
    final_table=safdv[safdv[!,:Int] .> 0, :]#removes the zeros again

    final_table.Nr=1:size(final_table,1) #adds the Nr column


    FileName1=filenames #unnecessary but left in for future purpose
    output=joinpath(pathin, FileName1) 
    output1= output * "_report.csv"
    CSV.write(output1, final_table)#writes the output to a csv file


    return (final_table)
    GC.gc()
end









function nozero(row2sort::Vector{Float32})
    vp=row2sort
    vz=zeros(Float32,length(vp))
    first =findfirst(x -> x >= 0,vp)
    if isnothing(first)
        return vz
    end
    last = findlast(x -> x>=0,vp)
    vp=vp[first:last]
    vz[1:length(vp)]=vp
    return vz
end


function split_setup(mz_val,mz_int,Rt,max_numb_iter,
  max_t_peak_w,res,min_ms_w,r_thresh,min_int,S2N,min_peak_w_s,splits_no)


    Max_it=repeat([max_numb_iter],splits_no)
    Max_t_peak_w=repeat([max_t_peak_w],splits_no)
    Res=repeat([res],splits_no)
    Min_ms_w=repeat([min_ms_w],splits_no)
    R_thresh=repeat([r_thresh],splits_no)
    Min_int=repeat([min_int],splits_no)

    S2N1=repeat([S2N],splits_no)
    Min_peak_w_s=repeat([min_peak_w_s],splits_no)

    Rt_mat=repeat([Rt],splits_no)
    GC.gc()
   

    Vb = Vector{Array}(undef,splits_no) #creates a vector to park the matrices into
    Vi=Vector{Array}(undef,splits_no) 
    C=Vector{Float32}(undef,splits_no) 
    mz=deepcopy(mz_val)#copies the original vectors
    mzint=deepcopy(mz_int)
    mz[mzint.<=min_int].=0 
    mzvec=vec(mz)
    mzvec=filter(!iszero, mzvec)
    sort!(mzvec)
    #Cleans up the masses to remove low intensity masses

    chunkB=Int(floor(length(mzvec)/splits_no)) #creates an integer to find the nth mass in the vector
    for i = 1:splits_no
    C[i]=mzvec[chunkB*i] #Sets the chunks into splits_no hopefully equally distributed mass sizes
    end
    GC.gc()
    
    for i = 1:splits_no
        mz=deepcopy(mz_val)#copies the original vectors
        mzint=deepcopy(mz_int)
        n=size(mz,2)
        m=size(mz,1)
    
        
        if i == 1
    
            mz=deepcopy(mz_val)
            mzint=deepcopy(mz_int)
            mz[mz.<=0].=(-1)
            mz[mz.>=(round(C[1],digits=2)+0.5)].=(-1)#smallest masses
    
    
            mzint[mz .< 0 ].=(-1)
            mz=mapslices(row -> nozero(row), mz, dims = 2)
            mzint=mapslices(row -> nozero(row), mzint, dims = 2)
            mz=mz[:, vec(mapslices(col -> any(col .!= 0), mz, dims = 1))]
            mzint=mzint[:, vec(mapslices(col -> any(col .!= 0), mzint, dims = 1))]

            Vi[i]=deepcopy(mzint)
            Vb[i]=deepcopy(mz)
            GC.gc()
    
    
        elseif i == splits_no
            mz=deepcopy(mz_val)
            mzint=deepcopy(mz_int)
            mz[mz.<=(round(C[(splits_no-1)],digits=2)-0.5)].=(-1)#largest masses
    
    
            mzint[mz .< 0 ].=(-1)
            mz=mapslices(row -> nozero(row), mz, dims = 2)
            mzint=mapslices(row -> nozero(row), mzint, dims = 2)
            mz=mz[:, vec(mapslices(col -> any(col .!= 0), mz, dims = 1))]
            mzint=mzint[:, vec(mapslices(col -> any(col .!= 0), mzint, dims = 1))]

            Vi[i]=deepcopy(mzint)
            Vb[i]=deepcopy(mz)
            GC.gc()
        else
            mz=deepcopy(mz_val)
            mzint=deepcopy(mz_int)
            bh=C[i]
            bl=C[(i-1)]
            mz[mz.<=(round(bl,digits=2)-0.5)].=(-1)
    
            mz[mz.>(round(bh,digits=2)+0.5)].=(-1)
    
    
            mzint[mz .< 0 ].=(-1)
            mz=mapslices(row -> nozero(row), mz, dims = 2)
            mzint=mapslices(row -> nozero(row), mzint, dims = 2)
            mz=mz[:, vec(mapslices(col -> any(col .!= 0), mz, dims = 1))]
            mzint=mzint[:, vec(mapslices(col -> any(col .!= 0), mzint, dims = 1))]

            Vi[i]=deepcopy(mzint)
            Vb[i]=deepcopy(mz)
            GC.gc()
        end
    end
    GC.gc()
    return (Vb,Vi,Rt_mat,Max_it,Max_t_peak_w,Res,Min_ms_w,R_thresh,Min_int,S2N1,Min_peak_w_s,C)
end
function safd_gc_mc(mz_vals,mz_int,Rt,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_thresh,min_int,S2N,min_peak_w_s)

        

    
    safdv1=safd_s3D_GC(mz_vals,mz_int,Rt,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_thresh,min_int,S2N,min_peak_w_s)
        GC.gc()


    return (safdv1)    

    
end
