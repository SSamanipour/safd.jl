using LsqFit
using Dierckx
using Statistics
using DataFrames
using CSV
using DSP
using BenchmarkTools
#using ProgressMeter
using ProgressBars
using Dates

#################################################################################

function max_sig_finder(mz_vals,mz_int,max_t_peak_w,min_int,bm)
    
    mind = argmax(mz_int .* bm)
    m = mz_int[mind]
    #m=mz_int[814,79]
    
    if m < min_int
        t_y_r = Matrix{Float32}(undef, 0, 0)
        t_x_r = Matrix{Float32}(undef, 0, 0)
        bounds = Vector{Int64}(undef, 0)
        #mind=[]
        ms_maxim = Float32(0.0)
        return (t_y_r,t_x_r,bounds,mind,ms_maxim)
    end
    pos1 = mind[1]-max_t_peak_w
    
    pos3 = mind[1]+max_t_peak_w 
    pos3 = min(pos3, size(mz_vals, 1))
    bounds_pos2 = min(mind[1], max_t_peak_w + 1)
        #mind=CartesianIndex(205,13824)
    pos1 = (pos1 >= 1) * pos1 + (pos1 <= 1) * 1
    
    t_y_r = mz_int[pos1:pos3, :]
    t_x_r = mz_vals[pos1:pos3, :]
    bounds = [pos1,bounds_pos2,pos3]
    ms_maxim = Float32(mz_vals[mind])
    bm_r = bm[pos1:pos3, :]

    return (t_y_r,t_x_r,bounds,mind,ms_maxim,bm_r)

end

#t_y_r,t_x_r,bounds,mind,ms_maxim=max_sig_finder(mz_vals,mz_int,max_t_peak_w,min_int)


####################################################################
#Simple moving average

function sma(y, n)

    vals = zeros(size(y,1) ,1)
    for i in 1:size(vals,1)-(n-1)
        vals[i+1] = mean(y[i:i+(n-1)])
    end

    vals[1]=y[1]
    vals[end]=y[end]
    return vals
end

####################################################################
#Rolling average smoothing
#n must be an odd number

function ra_smooth(y, n)

    w=convert(Int64,(n-1)/2)
    vals = zeros(size(y,1))

    for i=2:size(vals,1)-w
        tv1=y[i-w:i+w]
        vals[i] = mean(tv1)
    end

    vals[1]=y[1]
    vals[end]=y[end]
    return vals
end

####################################################################
#This function produced the mass vectors for limited to the peak
#
##


function res2mass_window(mz_vect,int_vect,res,min_ms_w,ms_maxim)
    tv1 = abs.(mz_vect .- ms_maxim)
    tv2 = length(tv1[tv1 .<= min_ms_w])

    #plot(tv1[tv1 .<= min_ms_w],filter(x->x <= min_ms_w,tv1))


    if  tv2 > 0

        # plot(int_vect)
        dms=2*(ms_maxim/res)
        if dms < min_ms_w
            dms = min_ms_w
        end
        x_t = mz_vect[tv1 .<= dms]
        y_t = int_vect[tv1 .<= dms]

        #plot(mz_vect[tv1 .<= dms/2],int_vect[tv1 .<= dms/2])

    else

        x_t=[]
        y_t=[]


    end
    return (x_t,y_t)


end



#x_t,y_t = res2mass_window(mz_vect,int_vect,res,min_ms_w,ms_maxim)
####################################################################
#
# typeof(findfirst(x -> x < maximum(y)/2,y2)) != Nothing

function isolate_sig(x,y)


    y1=y[1:argmax(y)]
    y2=y[argmax(y):end]
    if findlast(x -> x < maximum(y)/2,y1) != nothing && findfirst(x -> x < maximum(y)/2,y2) != nothing
        ys=y[findlast(x -> x < maximum(y)/2,y1):findfirst(x -> x < maximum(y)/2,y2)+argmax(y)-1]
        xs=x[findlast(x -> x < maximum(y)/2,y1):findfirst(x -> x < maximum(y)/2,y2)+argmax(y)-1]
    elseif findlast(x -> x < maximum(y)/2,y1) == nothing && findfirst(x -> x < maximum(y)/2,y2)!= nothing
        ys=y[1:findfirst(x -> x < maximum(y)/2,y2)+argmax(y)-1]
        xs=x[1:findfirst(x -> x < maximum(y)/2,y2)+argmax(y)-1]

    elseif findlast(x -> x < maximum(y)/2,y1) != nothing && findfirst(x -> x < maximum(y)/2,y2)== nothing
        ys=y[findlast(x -> x < maximum(y)/2,y1):end]
        xs=x[findlast(x -> x < maximum(y)/2,y1):end]
    else
        ys=y
        xs=x

    end

    if length(xs)>3

        spl=Spline1D(xs, ys; k=3, bc="nearest", s=0.0)
        x2=range(minimum(x),stop=maximum(x),length=50)
        y2=spl(x2)
        #y2_s=sma()
        y2_out=y2[y2.>=maximum(y)/2]
        x2_out=x2[y2.>=maximum(y)/2]
        res_m=x2[y2.==maximum(y2)]/(x2_out[end]-x2_out[1])
    elseif length(xs)-1 == 2
        spl=Spline1D(xs, ys; k=length(xs)-1, bc="nearest")
        x2=range(minimum(x),stop=maximum(x),length=50)
        y2=spl(x2)
        y2_out=y2[y2.>=maximum(y)/2]
        x2_out=x2[y2.>=maximum(y)/2]
        res_m=x2[y2.==maximum(y2)]/(x2_out[end]-x2_out[1])

    else
        y2_out=[]
        x2_out=[]
        res_m=[]

    end


    return(x2_out, y2_out,res_m)

end


# x2fit, y2fit, res_m=isolate_sig(x_t,y_t,min_int)


######################################################################

function simple_gauss(x2fit,y2fit,g_numb,r_thresh,min_ms_w)
    if g_numb == 1
        @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))

        p=zeros(3)
        p[1]=maximum(y2fit)
        p[2]=x2fit[argmax(y2fit)]
        p[3]=(maximum(x2fit)-minimum(x2fit))
        lbp=[0,x2fit[argmax(y2fit)]-min_ms_w,0]
        ubp=[Inf,x2fit[argmax(y2fit)]+min_ms_w,Inf]
        fit1=[]
        r2=0

        try
            fit1 = curve_fit(model ,x2fit, y2fit, p)
            r2=1 - var(fit1.resid) / var(y2fit)
            #println(r2)
        catch
            fit1=[]
            r2=0

        end
        #y_r=model(x_t,fit1.param)
        if r2 < r_thresh
            try
                fit1 = curve_fit(model ,x2fit, y2fit, p,lower=lbp, upper=ubp)
                r2=1 - var(fit1.resid) / var(y2fit)

            catch
                fit1=[]
                r2=0

            end

        end

        return (r2,fit1)
    elseif g_numb == 2
        @.model1(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))+
        (p[4]/(p[6]*sqrt(pi*2))) * exp(-(x-p[5])^2 / (2*p[6]^2))

        p=zeros(6)
        p[1]=maximum(y2fit)
        p[2]=x2fit[argmax(y2fit)]
        p[3]=(maximum(x2fit)-minimum(x2fit))
        p[4]=maximum(y2fit)
        p[5]=x2fit[argmax(y2fit)]
        p[6]=(maximum(x2fit)-minimum(x2fit))
        lbp=[0,x2fit[argmax(y2fit)]-min_ms_w,0,
        0,x2fit[argmax(y2fit)]-min_ms_w,0]
        ubp=[Inf,x2fit[argmax(y2fit)]+min_ms_w,Inf,
        Inf,x2fit[argmax(y2fit)]+min_ms_w,Inf]
        fit1=[]
        r2=0

        try
            fit1 = curve_fit(model1, x2fit, y2fit, p)
            r2=1 - var(fit1.resid) / var(y2fit)

        catch
            fit1=[]
            r2=0
        end

        if r2 < r_thresh

            try
                fit1 = curve_fit(model1 ,x2fit, y2fit, p,lower=lbp, upper=ubp)
                r2=1 - var(fit1.resid) / var(y2fit)
            catch
                fit1=[]
                r2=0

            end
        end


        return (r2,fit1)


    else
        println("The number of Gaussians to fit is limited to two.")
        r2=0
        fit=[]
        return (r2,fit)

    end

end

# r2,fit1=simple_gauss(x2fit,y2fit,1,r_thresh,min_ms_w)


######################################################################
#


function signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)

    if size(fit1.param,1) == 3
        @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2));
        y_r=model(x_t,fit1.param)
        #y_r1=model(x2fit,fit.param)
        x_max=x_t[argmax(y_r)]
        s_sig=length(y_r[y_r.>min_int/2])

    elseif size(fit1.param,1) == 6
        @.model1(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))+
        (p[4]/(p[6]*sqrt(pi*2))) * exp(-(x-p[5])^2 / (2*p[6]^2))
        y_r=model1(x_t,fit1.param)
        x_max=x_t[argmax(y_r)]
        s_sig=length(y_r[y_r.>min_int/2])

    end
    x_rep = x_t[y_r.> 0]
    y_rep = y_t[y_r.> 0]
    if length(y_r[y_r .> min_int/2]) == length(y_t[y_t .> min_int/2])
        y_t[y_t .> min_int/2] = (min_int/2)*ones(length(y_t[y_r .> min_int/2]))
    else
        y_t[y_t .> min_int/2]=(min_int/2)*ones(length(y_t[y_t .> min_int/2]))
    end


    return (x_rep,y_rep,y_t,x_max,s_sig)


end

#x_rep,y_rep,y,x_max,s_sig=signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)


#######################################################################


function peak_detect_n_interp(mz_vect,int_vect,res,min_ms_w,r_thresh,ms_maxim,min_int)

    x_t,y_t = res2mass_window(mz_vect,int_vect,res,min_ms_w,ms_maxim)

    if length(x_t)>0 && maximum(y_t)> min_int && length(y_t[ y_t .> min_int])>=2

        # plot(x_t,y_t)
        # plot(mz_vect,int_vect)
        # x = x_t
        # y = y_t

        x2fit, y2fit, res_m = isolate_sig_n_interp(x_t,y_t)
        # plot(x2fit, y2fit)

        if length(x2fit)>3 #&& maximum(y_t)/median(y_t)>=S2N
            g_numb=1
            r2,fit1=simple_gauss(x2fit,y2fit,g_numb,r_thresh,min_ms_w)

            if r2 <= r_thresh

                r2,fit1=simple_gauss(x2fit,sma(y2fit,3)[:],g_numb,r_thresh,min_ms_w)

            end

            if r2 <= r_thresh && length(x2fit)>5
                g_numb=2
                r2,fit1=simple_gauss(x2fit,y2fit,g_numb,r_thresh,min_ms_w)
            end


            if r2 >= r_thresh
                x_rep,y_rep,y,x_max,s_sig = signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)
            else
                #x_rep,y_rep,y,x_max,s_sig=signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)
                y_t=(min_int/2)*ones(length(y_t))
                y=y_t
                s_sig=maximum(y2fit)
                x_rep=x2fit
                y_rep=y2fit
                x_max=ms_maxim

            end
        elseif length(x2fit)>0 && length(x2fit) <= 3
            y_t=(min_int/2)*ones(length(y_t))
            y=y_t
            s_sig=maximum(y2fit)
            x_rep=x2fit
            y_rep=y2fit
            x_max=ms_maxim

        else
            y_t=(min_int/2)*ones(length(y_t))
            y=y_t
            s_sig=0
            x_rep=[]
            y_rep=[]
            x_max=[]
            res_m = []
        end


    else
        y_t = (min_int/2)*ones(length(y_t))
        y = y_t
        s_sig = 0
        x_rep =[]
        y_rep =[]
        x_max =[]
        res_m = []


    end

    return (x_rep,y_rep,y,x_t,x_max,s_sig,res_m)


end




#######################################################################


function peak_detect(mz_vect,int_vect,res,mind,min_ms_w,r_thresh,ms_maxim,min_int,S2N)

    x_t,y_t = res2mass_window(mz_vect,int_vect,res,min_ms_w,ms_maxim)

    if length(x_t)>0 && maximum(y_t)> min_int/2
        y_s=ra_smooth(y_t,3)
        #x=x_t
        #y=y_s[:,1]
        x2fit, y2fit, res_m=isolate_sig(x_t,y_s[:,1])
        if length(x2fit)>0 #&& maximum(y_t)/median(y_t)>=S2N
            g_numb=1
            r2,fit1=simple_gauss(x2fit,y2fit,g_numb,r_thresh,min_ms_w)
            if r2 <= r_thresh
                g_numb=2
                r2,fit1=simple_gauss(x2fit,y2fit,g_numb,r_thresh,min_ms_w)
            end


            if r2 >= r_thresh
                x_rep,y_rep,y,x_max,s_sig=signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)
            else
                #x_rep,y_rep,y,x_max,s_sig=signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)
                y_t=(min_int/2)*ones(length(y_t))
                y=y_t
                s_sig=0
                x_rep=[]
                y_rep=[]
                x_max=[]

            end
        else
            y_t=(min_int/2)*ones(length(y_t))
            y=y_t
            s_sig=0
            x_rep=[]
            y_rep=[]
            x_max=[]


        end


    else
        y_t=(min_int/2)*ones(length(y_t))
        y=y_t
        s_sig=0
        x_rep=[]
        y_rep=[]
        x_max=[]


    end

    return (x_rep,y_rep,y,x_t,x_max,s_sig)


end

#######################################################################
#

function feature_isolate!(bounds,t_x_r,t_y_r,res,mind,min_ms_w,r_thresh,ms_maxim,
    sig_inc_thresh,min_int,S2N)

    mz_vect=t_x_r[bounds[2],:]
    int_vect=t_y_r[bounds[2],:]
    #plot(mz_vect,int_vect)
    x_rep,y_rep,y,x_t,x_max,s_sig=peak_detect(mz_vect,int_vect,res,mind,
    min_ms_w,r_thresh,ms_maxim,min_int,S2N)
    feature=zeros(size(t_x_r,1),10*length(y))
    masses=zeros(size(t_x_r,1),10*length(y))
    y_f=zeros(size(t_x_r,1),10*length(y))
    if length(x_rep)>0
        in_ind=convert(Int32,ceil((length(y)/2)-(size(x_rep,1)/2)))
        if in_ind > 0
            feature[bounds[2],in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[bounds[2],in_ind:(in_ind+size(x_rep,1))-1]=x_rep
        elseif in_ind == 0

            feature[bounds[2],in_ind+1:(in_ind+size(x_rep,1))]=y_rep
            masses[bounds[2],in_ind+1:(in_ind+size(x_rep,1))]=x_rep

        end

    end

    if length(y)==length(t_y_r[1,:])
        t_y_r[bounds[2],:]=y
    else
        ln=findfirst(isequal(minimum(x_t)),mz_vect)
        hn=findfirst(isequal(maximum(x_t)),mz_vect)
        t_y_r[bounds[2],ln:hn]=y
    end

    mz_vect=[]
    int_vect=[]
    ind_f=bounds[3]-bounds[1]-bounds[2]
    ind_rev=bounds[3]-bounds[1]-ind_f-1

    for i=1:ind_f

        #i=1
        #println(i)

        mz_vect=t_x_r[convert(Int32,bounds[2]+i),:]
        int_vect=t_y_r[convert(Int32,bounds[2]+i),:]

        x_rep,y_rep,y,x_t,x_max,s_sig=peak_detect(mz_vect,int_vect,res,mind,
        min_ms_w,r_thresh,ms_maxim,min_int,S2N)

        #plot(x_rep,y_rep)


        if bounds[2]+i-2 >0 && s_sig > 0 && maximum(y_rep)/maximum(feature[convert(Int32,bounds[2]+i-1),:]) <= ((100+sig_inc_thresh)/100)


            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            #println(y_rep)
            # size(feature)
            feature[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(y_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]+i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]

            #plot(masses[convert(Int32,bounds[2]+i),:],feature[convert(Int32,bounds[2]+i),:])

        elseif bounds[2]+i-2 >0 && s_sig > 0 && maximum(y_rep)<3*min_int && abs(maximum(y_rep)-maximum(feature[convert(Int32,bounds[2]+i-1),:])) <= ((3*sig_inc_thresh)/100)*min_int

            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            #println(y_rep)
            feature[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]+i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]

        elseif bounds[2]+i-2 > 0 && s_sig > 0 && maximum(y_rep)/maximum(feature[convert(Int32,bounds[2]+i-2),:]) <= ((100+sig_inc_thresh)/100)

            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            #println(y_rep)
            feature[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]+i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
        elseif s_sig == 0 && length(x_rep) < 1 && length(y) == length(t_y_r[1,:])
            t_y_r[convert(Int32,bounds[2]+i),:]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break

        elseif s_sig == 0 && length(x_rep) < 1 && length(y) > 0 && length(y) < length(t_y_r[1,:])
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]+i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break

        elseif s_sig == 0 && length(x_rep) < 1 && length(y) ==0
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break

        end

    end

    for i=1:ind_rev
        #println(i)

        if convert(Int32,bounds[2]-i)>0
            mz_vect=t_x_r[convert(Int32,bounds[2]-i),:]
            int_vect=t_y_r[convert(Int32,bounds[2]-i),:]
            ind2=convert(Int32,bounds[2]-i)
        elseif convert(Int32,bounds[2]-i)==0
            mz_vect=t_x_r[convert(Int32,1),:]
            int_vect=t_y_r[convert(Int32,1),:]
            ind2=1
        end

        x_rep,y_rep,y,x_t,x_max,s_sig=peak_detect(mz_vect,int_vect,res,mind
        ,min_ms_w,r_thresh,ms_maxim,min_int,S2N)

        #println(y_rep)

        if s_sig > 0 &&  maximum(y_rep)/maximum(feature[convert(Int32,bounds[2]-i+1),:]) <= ((100+sig_inc_thresh)/100)
            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            feature[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]-i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]

            #plot(masses[convert(Int32,bounds[2]+i),:],feature[convert(Int32,bounds[2]+i),:])
        elseif s_sig > 0 && maximum(y_rep)<3*min_int && abs(maximum(y_rep)-maximum(feature[convert(Int32,bounds[2]-i+1),:])) <= ((3*sig_inc_thresh)/100)*min_int
            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            feature[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]-i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]

        elseif s_sig > 0 &&  maximum(y_rep)/maximum(feature[convert(Int32,bounds[2]-i+2),:]) <= ((100+sig_inc_thresh)/100)
            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            feature[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]-i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]

        elseif s_sig == 0 && length(x_rep) < 1 && length(y) == length(t_y_r[1,:])

            t_y_r[ind2,:]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break
        elseif s_sig == 0 && length(x_rep) < 1 && length(y) > 0 &&length(y) < length(t_y_r[1,:])
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[ind2,ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break
        elseif s_sig == 0 && length(x_rep) < 1 && length(y) == 0
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break

        end

    end

    return (feature,masses,t_y_r)



end

#masses[masses.>0]

###################################################
#Resolution calculator
#  i=3
# tv2[13]

function res_calc(feature,masses)
    resolution=zeros(size(feature,1))

    for i=1:size(feature,1)
        ttv1=feature[i,:]
        ttv2=masses[i,:]
        tv1=ttv1[ttv1.>0]
        tv2=ttv2[ttv2.>0]
        #plot(tv1)
        if size(tv1[tv1 .> 0 ],1)>1
            m_max=maximum(tv1)
            lb_m=findfirst(x -> x >= m_max/2,tv1)
            hb_m=findlast(x -> x >= m_max/2,tv1[lb_m:end])
            if hb_m+lb_m <= length(tv1) && tv2[hb_m+lb_m] > 0
                dm_meas=tv2[hb_m+lb_m]-tv2[lb_m]
            else
                dm_meas=tv2[hb_m+lb_m-1]-tv2[lb_m]
            end

            if isnan(tv2[argmax(tv1)]/dm_meas*1) .==0 && isequal(tv2[argmax(tv1)]/dm_meas,missing)==0
                resolution[i]=round(tv2[argmax(tv1)]/dm_meas)
            else
                resolution[i]=0
            end


        end
        #println(resolution[i])
    end
    return resolution

end

#plot(resolution)
###################################################
#The feature detection function


function feature_detect(Rt,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,
    r_thresh,sig_inc_thresh,S2N,min_peak_w_s)

    t_y_r,t_x_r,bounds,mind,ms_maxim =max_sig_finder(mz_vals,mz_int,max_t_peak_w,min_int)



    if size(t_y_r,1) >0

        feature,masses,t_y_r=feature_isolate!(bounds,t_x_r,t_y_r,res,mind,min_ms_w,
        r_thresh,ms_maxim,sig_inc_thresh,min_int,S2N)

        if size(feature,1) > 1
            t_dim=zeros(size(feature,1))

            for i=1:size(feature,1)
                tv1=feature[i,:]

                if size(tv1[tv1 .> 0 ],1)>1

                    t_dim[i]=tv1[argmin(abs.(masses[i,:] .-  ms_maxim))]

                end
            end

            if size(t_dim[t_dim .> 0],1)>1
                min_mass=minimum(masses[masses.>0])
                max_mass=maximum(masses[masses.>0])

                signal=zeros(size(t_x_r,1))

                for i=1:length(signal)
                    ind_mass=findall(x -> max_mass>=x>=min_mass,t_x_r[i,:])
                    if length(ind_mass)>1
                        signal[i]=maximum(t_y_r[i,ind_mass])
                    end
                    #println(i)

                end

                signal_f=signal+t_dim
                signal_f[signal_f.!=min_int/2]
            else
                signal_f=Inf
            end

            #i=12

            #plot(t_dim)
            #plot(signal_f)
            #scatter!(t_dim)
            #plot(signal)

            if size(t_dim[t_dim .> 0],1)>1 &&
                length(t_dim[t_dim.>min_int/2])>= min_peak_w_s &&
                 round(maximum(t_dim)/  mean(signal_f)) > S2N

                t_dim2fit=ra_smooth(t_dim[t_dim.>=min_int/2],3)
                # plot(t_dim2fit)
                #plot!(sma(t_dim2fit,3))
                g_numb=1
                #r2,fit1=simple_gauss(range(1,stop=length(t_dim)),t_dim,g_numb,r_thresh,min_ms_w)
                r2,fit1=simple_gauss(range(1,stop=length(t_dim2fit)),t_dim2fit,g_numb,r_thresh,min_ms_w)
                #Run the model first

                if r2 < r_thresh && length(t_dim2fit)>7
                    g_numb=2
                    #r2,fit1=simple_gauss(range(1,stop=length(t_dim)),t_dim,g_numb,r_thresh,min_ms_w)
                    r2,fit1=simple_gauss(range(1,stop=length(t_dim2fit)),t_dim2fit,g_numb,r_thresh,min_ms_w)
                end

                if length(r2) > 0 && r2 >= r_thresh && length(t_dim[t_dim .> 0]) > min_peak_w_s



                    resolution=res_calc(feature,masses)
                    resolution[isnan.(resolution)].=0
                    resolution[isinf.((resolution))].=0
                    #plot(resolution)
                    med_res=round(mean(resolution[resolution .> 0]))
                    ind=argmax(t_dim)
                    lb=findlast(x -> x < min_int,t_dim[1:ind[1]])
                    ub=findfirst(x -> x <= min_int,t_dim[ind[1]:end])-1

                    scan_num=mind[1]
                    p_w_s = (ind -lb) + ub 
                    ret_t = Rt[mind[1]]

                    #println(mind[1] - Int(round(length(t_dim)/4)))

                    if mind[1] + Int(round(length(t_dim)/4)) < length(Rt) && (mind[1] - Int(round(length(t_dim)/4))) > 2
                        rt_i = Rt[mind[1] - (ind -lb)]
                        rt_e = Rt[mind[1] + ub]
                    elseif mind[1] + Int(round(length(t_dim)/4)) >= length(Rt) && (mind[1] - Int(round(length(t_dim)/4))) > 2
                        rt_i = Rt[mind[1] - (ind -lb)]
                        rt_e=Rt[end]
                    elseif (mind[1] - Int(round(length(t_dim)/4))) <=2
                        #println(Rt[2:4])
                        rt_i=Rt[2]
                        rt_e = Rt[mind[1] + ub]
                    end
                    p_w_t=round((rt_e -rt_i), digits=2)
                    mass_meas=round(mean(masses[masses .> 0]),digits=4)
                    min_mass=round(minimum(masses[masses .> 0]),digits=4)
                    max_mass=round(maximum(masses[masses .> 0]),digits=4)
                    feat_int=round(sum(feature))
                    feat_purity=g_numb
                    sig=maximum(feature)
                else
                    scan_num=mind[1]
                    p_w_s=[]
                    ret_t=[]
                    rt_i=[]
                    rt_e=[]
                    p_w_t=[]
                    mass_meas=[]
                    min_mass=[]
                    max_mass=[]
                    feat_int=[]
                    feat_purity=[]
                    med_res =[]
                    sig=maximum(feature)

                end

            else
                scan_num=mind[1]
                p_w_s=[]
                ret_t=[]
                rt_i=[]
                rt_e=[]
                p_w_t=[]
                mass_meas=[]
                min_mass=[]
                max_mass=[]
                feat_int=[]
                feat_purity=[]
                med_res =[]
                sig=maximum(feature)
            end

        else
            scan_num=mind[1]
            p_w_s=[]
            ret_t=[]
            p_w_t=[]
            mass_meas=[]
            min_mass=[]
            max_mass=[]
            feat_int=[]
            feat_purity=[]
            med_res =[]
            sig=maximum(feature)


        end
    else
        scan_num=[]
        p_w_s=[]
        ret_t=[]
        p_w_t=[]
        mass_meas=[]
        min_mass=[]
        max_mass=[]
        feat_int=[]
        feat_purity=[]
        med_res =[]
        sig=min_int

    end


    return (sig,t_y_r,bounds,scan_num,p_w_s,ret_t,rt_i,rt_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
    feat_purity,med_res)

end




######################################################################
## Wrapper function


function safd(mz_vals::Array{Float32,2},mz_int::Array{Float32,2},Rt::Array{Float32,1},
    FileName::Any,path::String,max_numb_iter::Int64,
    max_t_peak_w::Int64,res::Int64,min_ms_w::Float64,r_thresh::Float64,
    min_int::Int64,sig_inc_thresh::Int64,S2N::Int64,min_peak_w_s::Int64)

    #hz=(size(mz_int,1)/(60*(t_end-t0)))
    rep_table=zeros(max_numb_iter,14)

    for i in ProgressBar(1:max_numb_iter)
        sig,t_y_r,bounds,scan_num,p_w_s,ret_t,rt_i,rt_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
        feat_purity,med_res = feature_detect(Rt,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,
            r_thresh,sig_inc_thresh,S2N,min_peak_w_s)

        if length(med_res) > 0 && sig > min_int



            rep=[scan_num,p_w_s,ret_t,rt_i,rt_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
            sig,feat_purity,med_res]
            rep_table[i,2:end]=rep
            #println([i,scan_num,mass_meas,sig,med_res])

        elseif sig>0 && sig <= min_int
            break

        end
        mz_int[bounds[1]:bounds[3],:]=t_y_r
    end


    table = DataFrame(rep_table,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes])
    sort!(table,[:Rt,:MeasMass])


    final_table=table[table[!,:Int] .> 0,:]
    final_table.Nr=1:size(final_table,1)

    output=joinpath(path, FileName)
    output1= output * "_report.csv"
    CSV.write(output1,final_table)
    println("The final report has been saved in the output path!")

    return (rep_table,final_table)

end

######################################################################
## cummulative mean

function cummean(XIC)
    cm = zeros(size(XIC))
    cm[1] = XIC[1]

    for i=2:length(XIC)
        cm[i] = mean(XIC[1:i])
    end
    return cm

end


######################################################################
## Pseudo feature detection

function feature_detect_s3D(Rt,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,
    r_thresh,S2N,bm)

    #mz_vals,mz_int,t0,t_end=import_files(format,numchan,namechan,pathin,filenames)
    #  mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,Rt=import_files_MS1(pathin,filenames,mz_thresh)

    t_y_r,t_x_r,bounds,mind,ms_maxim,bm_r = max_sig_finder(mz_vals,mz_int,max_t_peak_w,min_int,bm)

    if size(t_y_r,1)>0
        mz_vect = t_x_r[bounds[2],:]
        int_vect = t_y_r[bounds[2],:]
        # plot(mz_vect,int_vect)
        x_rep,y_rep,y,x_t,x_max,s_sig,res_m = peak_detect_n_interp(mz_vect,int_vect,res,min_ms_w,r_thresh,ms_maxim,min_int)

        # plot(x_rep,y_rep)
        # i=301
        del_m = abs.(t_x_r .- ms_maxim)
        t_y_r_t = deepcopy(t_y_r)

        mz_tol = (maximum(x_t) - minimum(x_t))/2

        t_y_r_t[del_m .> mz_tol] .= 0
        XIC = maximum(t_y_r_t,dims =2)

        # plot(XIC)

        sig = XIC .- maximum(cummean(XIC))
        # plot!(sig)

        if isnothing(findlast(x -> x <=0, sig[1:argmax(sig)[1]]))==0 && isnothing( findfirst(x -> x <=0, sig[argmax(sig)[1]:end]))==0

            lb = findlast(x -> x <=0, sig[1:argmax(sig)[1]])
            ub = argmax(sig)[1] + findfirst(x -> x <=0, sig[argmax(sig)[1]:end]) - 1
        elseif isnothing(findlast(x -> x <=0, sig[1:argmax(sig)[1]]))==0 && isnothing( findfirst(x -> x <=0, sig[argmax(sig)[1]:end]))> 0
            lb = findlast(x -> x <=0, sig[1:argmax(sig)[1]])
            ub = size(t_y_r,1)
        elseif isnothing(findlast(x -> x <=0, sig[1:argmax(sig)[1]]))> 0 && isnothing( findfirst(x -> x <=0, sig[argmax(sig)[1]:end]))==0
            ub = argmax(sig)[1] + findfirst(x -> x <=0, sig[argmax(sig)[1]:end]) - 1
            lb = 1
        else
            ub = 1
            lb = size(t_y_r,1)

        end

        t_y_r_t2 = t_y_r[lb:ub,:]
        bm_r2 = bm_r[lb:ub,:]
        del_m2 = del_m[lb:ub,:]
        feature = t_y_r_t2[del_m2 .<= mz_tol]
        bm_r2[del_m2 .<= mz_tol] .= 0

        bm_r[lb:ub,:] = bm_r2

        if s_sig >0 && maximum(XIC)/maximum(cummean(XIC)) >= S2N


            y = sma(XIC[lb:ub],3);

            #mz_int[bounds[1]:bounds[3],:] = t_y_r
            bm[bounds[1]:bounds[3],:] = bm_r

            g_numb,r2 = simple_gauss_t(y[:],r_thresh)

            if r2>=r_thresh

                p_w_s=length(y)
                t_i = Rt[bounds[1] + lb]
                t_e = Rt[bounds[1] + ub - 1]
                scan_num = mind[1]
                ret_t = Rt[mind[1]]
                p_w_t = round(t_e - t_i,digits=2)
                mass_meas = round(mean(x_t),digits=4)
                min_mass = round(minimum(x_t),digits=4)
                max_mass = round(maximum(x_t),digits=4)
                feat_int = round(maximum(XIC[lb:ub,:]))
                feat_purity = g_numb
                sig_r = sum(XIC[lb:ub,:])
                med_res = res_m
            else
                scan_num=mind[1]
                t_i=[]
                t_e=[]
                p_w_s=[]
                ret_t=[]
                p_w_t=[]
                mass_meas=[]
                min_mass=[]
                max_mass=[]
                feat_int=[]
                feat_purity=[]
                sig_r = sum(XIC[lb:ub,:])
                med_res=[]


            end
        else
            scan_num = mind[1]
            p_w_s=[]
            ret_t=[]
            t_i=[]
            t_e=[]
            p_w_t=[]
            mass_meas=[]
            min_mass=[]
            max_mass=[]
            feat_int=[]
            feat_purity=[]
            sig_r = sum(fXIC[lb:ub,:])
            med_res=[]
            #mz_int[bounds[1]:bounds[3],:]=t_y_r
            bm[bounds[1]:bounds[3],:] = bm_r
        end

    else
        scan_num=mind[1]
        p_w_s=[]
        ret_t=[]
        t_i=[]
        t_e=[]
        p_w_t=[]
        mass_meas=[]
        min_mass=[]
        max_mass=[]
        feat_int=[]
        feat_purity=[]
        sig_r = 0
        med_res=[]


    end

    return(sig_r,mz_int,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
    feat_purity,med_res,bm)

end # function

##############################################################################

function isolate_sig_n_interp(x,y)


    m_y = maximum(y)
    m_y_d = y .- m_y/2

    x2_out = x[m_y_d .>= 0]
    y2_out = y[m_y_d .>= 0]
    if length(x2_out) > 0
        res_m = round(x[argmax(y)]/(x2_out[end]-x2_out[1]))
    else
        res_m = []
    end

    return(x2_out, y2_out,res_m)

end




##############################################################################
# Simple Gaussian function time domain

function simple_gauss_t(y,r_thresh)
    @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))

    x=range(1, stop=length(y))
    p=zeros(3)
    p[1]=maximum(y)
    p[2]=x[argmax(y)]
    p[3]=(maximum(x)-minimum(x))

    fit1=[]
    r2=0
    g_numb=[]

    try
        fit1 = curve_fit(model ,1:length(y), y, p)
        r2=round(1 - var(fit1.resid) / var(y); digits=2)
        g_numb=1
        #println(r2)
    catch
        fit1=[]
        r2=0
        g_numb=1

    end
    if r2<r_thresh
        @.model1(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))+
        (p[4]/(p[6]*sqrt(pi*2))) * exp(-(x-p[5])^2 / (2*p[6]^2))

        p=zeros(6)
        p[1]=maximum(y)
        p[2]=x[argmax(y)]
        p[3]=(maximum(x)-minimum(x))
        p[4]=maximum(y)
        p[5]=x[argmax(y)]
        p[6]=(maximum(x)-minimum(x))

        fit1=[]
        r2=0

        # plot(y)
         # plot!(model1(x,fit1.param))
        try
            fit1 = curve_fit(model1 ,1:length(y), y, p)
            r2=round(1 - var(fit1.resid) / var(y); digits=2)
            g_numb=2

        catch
            fit1=[]
            r2=0
            g_numb=2
        end
    end

    return(g_numb,r2)


end # function


######################################################################
## Wrapper function

function safd_s3D(mz_vals::Array{Float32,2},mz_int::Array{Float32,2},Rt::Array{Float32,1},
    FileName::Any,path::String,max_numb_iter::Int64,
    max_t_peak_w::Int64,res::Int64,min_ms_w::Float64,r_thresh::Float64,
    min_int::Int64,sig_inc_thresh::Int64,S2N::Int64,min_peak_w_s::Int64;kwargs...)

    #hz=(size(mz_int,1)/(60*(t_end-t0)))
    rep_table=zeros(max_numb_iter,14);
    bm = zeros(Int32,size(mz_vals))
    bm[mz_int .>= min_int] .= 1
    

    # i=1

    for i in ProgressBar(1:max_numb_iter, printing_delay=0.1)

    #@time for i=1:10#max_numb_iter
        #println(i)
        sig,mz_int,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_int,
        feat_purity,med_res, bm = feature_detect_s3D(Rt,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,
        r_thresh,S2N,bm);

        println([scan_num,mass_meas,sig])

        if length(med_res) > 0 && med_res[1] > 0 && sig > min_int

            rep=[scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,sig,feat_int,
            feat_purity,med_res[1]]
            rep_table[i,2:end]=rep


        elseif sig>=0 && sig <= min_int
            break

        end
    end


    table=DataFrame(rep_table,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes])
    sort!(table,[:Rt,:MeasMass])



    final_table = table[table[!,:Int] .> 0,:]
    final_table.Nr=1:size(final_table,1)

    output=joinpath(path, FileName)
    output1= output * "_report.csv"
    CSV.write(output1,final_table)

    if (@isdefined kwargs) && any(keys(kwargs) .== :output_settings) && lowercase(kwrags[:output_settings]) == "specific"
        #save settings for single file
        mzt1 = minimum(mz_vals)
        mzt2 = maximum(mz_vals)
        open(joinpath(path,FileName*"_SAFD settings.txt"), "w") do io
            write(io,  "Processing date = "*Dates.format(now(),"YYYY-mm-dd HH:MM")*"\nProcessing path = "*path*"\nProcessing file = "*FileName*"\nRange m/z = $mzt1 - $mzt2\n"*
                       "Feature detection vars\n\tmax_numb_iter = $max_numb_iter\n\tres = $res\n\tr_thresh = $r_thresh\n\tmax_t_peak_w = $max_t_peak_w\n\tmin_ms_w = $min_ms_w\n\tmin_int = $min_int\n\tsig_inc_thresh = $sig_inc_thresh\n\tS2N = $S2N\n\tmin_peak_w_s = $min_peak_w_s\n")
        end
    elseif (@isdefined kwargs) && any(keys(kwargs) .== :output_settings) && lowercase(kwrags[:output_settings]) == "general"
        #save general settings
        mzt1 = minimum(mz_vals)
        mzt2 = maximum(mz_vals)
        open(joinpath(path,"SAFD settings.txt"), "w") do io
            write(io,  "Processing date = "*Dates.format(now(),"YYYY-mm-dd HH:MM")*"\nProcessing path = "*path*"\nRange m/z = $mzt1 - $mzt2\n"*
                       "Feature detection vars\n\tmax_numb_iter = $max_numb_iter\n\tres = $res\n\tr_thresh = $r_thresh\n\tmax_t_peak_w = $max_t_peak_w\n\tmin_ms_w = $min_ms_w\n\tmin_int = $min_int\n\tsig_inc_thresh = $sig_inc_thresh\n\tS2N = $S2N\n\tmin_peak_w_s = $min_peak_w_s\n")
        end 
    end

    println("The final report has been saved in the output path!")



    return (rep_table,final_table)



end # function



######################################################################
## Wrapper function centroided SAFD

function safd_s3d_cent(mz_vals::Array{Float32,2},mz_int::Array{Float32,2},Rt::Array{Float32,1},
    FileName::Any,path::String,max_numb_iter::Int64,
    max_t_peak_w::Int64,res::Int64,min_ms_w::Float64,r_thresh::Float64,
    min_int::Int64,sig_inc_thresh::Int64,S2N::Int64,min_peak_w_s::Int64,method::String="RFM",mdm::Any=[];kwargs...)

    rep_table=zeros(max_numb_iter,14);
    

    if method == "BG" && length(mdm) == 0
        println("The algorithm will use the best guess based on the provided resolution.")
        dm = 0
    elseif method ==  "RFM" && length(mdm) == 0
        println("The delta m/z values will be predicted using a random forest model.")
        println("This will take several minutes!")
        dm=dmz_pred_batch(mz_vals,mz_int);

    elseif method == "MDM" && length(mdm) >0
        println("The algorithm will use the provided d m/z values.")
        dm = mdm
    elseif method == "CT" && length(mdm) == 1
        println("The algorithm will use the constant delta m/z values.")
        dm = mdm
    else
        println("The algorithm will use the best guess based on the provided resolution.")
        dm = 0

    end


    for i in ProgressBar(1:max_numb_iter, printing_delay=0.1)

    #for i=1:max_numb_iter
    #    println(i)
        sig,mz_int,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_area,
        feat_purity,med_res = feature_detect_s3D_cent(Rt,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,r_thresh,S2N,dm)

        if length(med_res) > 0 && med_res[1] > 0 && sig > min_int

            rep=[scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_area,
            sig,feat_purity,med_res[1]]
            rep_table[i,2:end]=rep


        elseif sig>=0 && sig <= min_int
            break

        end




    end

    table=DataFrame(rep_table,[:Nr,:ScanNum,:ScanInPeak,:Rt,:RtStart,:RtEnd,:MinInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes])
    sort!(table,[:Rt,:MeasMass])



    final_table=table[table[!,:Int] .> 0,:]
    final_table.Nr=1:size(final_table,1)

    output=joinpath(path, FileName)
    output1= output * "_Cent_report.csv"
    CSV.write(output1,final_table)

    if (@isdefined kwargs) && any(keys(kwargs) .== :output_settings) && lowercase(kwrags[:output_settings]) == "specific"
        #save settings for single file
        mzt1 = minimum(mz_vals)
        mzt2 = maximum(mz_vals)
        open(joinpath(path,FileName*"_SAFD settings.txt"), "w") do io
            write(io,  "Processing date = "*Dates.format(now(),"YYYY-mm-dd HH:MM")*"\nProcessing path = "*path*"\nProcessing file = "*FileName*"\nRange m/z = $mzt1 - $mzt2\n"*
                       "Feature detection vars\n\tmax_numb_iter = $max_numb_iter\n\tres = $res\n\tr_thresh = $r_thresh\n\tmax_t_peak_w = $max_t_peak_w\n\tmin_ms_w = $min_ms_w\n\tmin_int = $min_int\n\tsig_inc_thresh = $sig_inc_thresh\n\tS2N = $S2N\n\tmin_peak_w_s = $min_peak_w_s\n")
        end
    elseif (@isdefined kwargs) && any(keys(kwargs) .== :output_settings) && lowercase(kwrags[:output_settings]) == "general"
        #save general settings
        mzt1 = minimum(mz_vals)
        mzt2 = maximum(mz_vals)
        open(joinpath(path,"SAFD settings.txt"), "w") do io
            write(io,  "Processing date = "*Dates.format(now(),"YYYY-mm-dd HH:MM")*"\nProcessing path = "*path*"\nRange m/z = $mzt1 - $mzt2\n"*
                       "Feature detection vars\n\tmax_numb_iter = $max_numb_iter\n\tres = $res\n\tr_thresh = $r_thresh\n\tmax_t_peak_w = $max_t_peak_w\n\tmin_ms_w = $min_ms_w\n\tmin_int = $min_int\n\tsig_inc_thresh = $sig_inc_thresh\n\tS2N = $S2N\n\tmin_peak_w_s = $min_peak_w_s\n")

        end
    end

    println("The final report has been saved in the output path!")

    return (rep_table,final_table)

end

######################################################################
## Pseudo feature detection

function feature_detect_s3D_cent(Rt,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,
    r_thresh,S2N,dm)

    t_y_r,t_x_r,bounds,mind,ms_maxim = max_sig_finder(mz_vals,mz_int,max_t_peak_w,min_int)



    if size(t_y_r,1)>0
        if dm == 0
            dm_s = ms_maxim/res
            if dm_s < min_ms_w
                dm_s = min_ms_w
            end

        elseif length(dm) >1 && size(dm,2) >1
            dm_s = dm[mind]/1000
            #println("RFM")

        elseif length(dm) >1 && size(dm,2) ==1

            dm_s = 2*dm[mind[1]][mind[2]]
            #println("MDM")

        elseif length(dm) ==1
            if dm <1
                dm_s = dm
            else
                dm_s = dm/1000
            end
        else
            dm_s = ms_maxim/res
            if dm_s < min_ms_w
                dm_s = min_ms_w
            end

        end

        XIC,feature,ms_feature = peak_detect_n_interp_cent(t_y_r,t_x_r,ms_maxim,min_int,dm_s)
        # plot(XIC)


        ind=argmax(XIC)
        s_sig = maximum(XIC)

        if length(XIC[XIC .> 0]) > 0 && median(XIC[XIC .> 0 ]) > min_int
            md=mean(XIC[XIC .> 0])
        else
            md=min_int
        end

        if s_sig >0 && findfirst(x -> x <= md,XIC[ind[1]:end])!= nothing &&
             findlast(x -> x < md,XIC[1:ind[1]]) != nothing && maximum(XIC)/median(XIC[XIC .>= 0])>=S2N

            lb=findlast(x -> x < md,XIC[1:ind[1]])
            ub=ind[1]+findfirst(x -> x <= md,XIC[ind[1]:end])-1
            y=XIC[lb:ub];

            # plot(y)

            mz_int[bounds[1]:bounds[3],:]=t_y_r

            g_numb,r2=simple_gauss_t(y,r_thresh)

            if r2>=r_thresh

                scan_num=mind[1]
                p_w_s=length(y)
                if mind[1]+ Int(round(length(y)/4)) < length(Rt) && mind[1]- Int(round(length(y)/4)) > 2
                    t_i=Rt[mind[1]- Int(round(length(y)/4))]
                    t_e=Rt[mind[1]+ Int(round(length(y)/4))]
                elseif mind[1]+ Int(round(length(y)/4)) >= length(Rt) && mind[1]- Int(round(length(y)/4)) > 2
                    t_i=Rt[mind[1]- Int(round(length(y)/4))]
                    t_e=Rt[end]
                elseif mind[1]- Int(round(length(y)/4)) <= 2
                    #println(Rt[2:4])
                    t_i=Rt[2]
                    t_e=Rt[mind[1]+ Int(round(length(y)/4))]
                end
                ret_t=Rt[mind[1]]
                p_w_t=round(t_e - t_i,digits=2)
                mass_meas=round(mean(ms_feature[ms_feature .>0]),digits=4)
                min_mass=round(minimum(ms_feature[ms_feature .>0]),digits=4)
                max_mass=round(maximum(ms_feature[ms_feature .>0]),digits=4)
                feat_area=round(sum(feature[lb:ub,:]))
                feat_purity=g_numb
                sig=maximum(feature[lb:ub,:])
                med_res = round(2*mass_meas/(dm_s))
            else
                scan_num=mind[1]
                t_i=[]
                t_e=[]
                p_w_s=[]
                ret_t=[]
                p_w_t=[]
                mass_meas=[]
                min_mass=[]
                max_mass=[]
                feat_area=[]
                feat_purity=[]
                sig=maximum(feature[lb:ub,:])
                med_res=[]


            end
        else
            scan_num=mind[1]
            p_w_s=[]
            ret_t=[]
            t_i=[]
            t_e=[]
            p_w_t=[]
            mass_meas=[]
            min_mass=[]
            max_mass=[]
            feat_area=[]
            feat_purity=[]
            sig=maximum(t_y_r)
            med_res=[]
            mz_int[bounds[1]:bounds[3],:]=t_y_r
        end

    else
        scan_num=mind[1]
        p_w_s=[]
        ret_t=[]
        t_i=[]
        t_e=[]
        p_w_t=[]
        mass_meas=[]
        min_mass=[]
        max_mass=[]
        feat_area=[]
        feat_purity=[]
        sig=0
        med_res=[]


    end

    return(sig,mz_int,bounds,scan_num,p_w_s,ret_t,t_i,t_e,p_w_t,mass_meas,min_mass,max_mass,feat_area,
    feat_purity,med_res)

end # function



#######################################################################


function peak_detect_n_interp_cent(t_y_r,t_x_r,ms_maxim,min_int,dm_s)

    tv1 = abs.(t_x_r .- ms_maxim)

    #tv2 = t_y_r[findall(x->x <= dm,tv1)]

    XIC=zeros(size(t_y_r,1),1);
    feature=zeros(size(t_y_r,1),10);
    ms_feature = zeros(size(t_y_r,1),10);

    for i=1:size(t_y_r,1)
        #println(i)
        tv2 = t_y_r[i,findall(x -> 2*dm_s >= x ,tv1[i,:])]
        if length(tv2) > 0
            feature[i,1:length(tv2)]=tv2
            tv3 = t_x_r[i,findall(x -> 2*dm_s >= x ,tv1[i,:])]
            ms_feature[i,1:length(tv2)] = tv3
            t_y_r[i,findall(x -> 2*dm_s >= x ,tv1[i,:])] .= min_int/2
            if (length(tv1)>0)

                XIC[i] = maximum(tv2)
            end
        end
    end





    return (XIC,feature,ms_feature)


end


###################################################################################

"""
# test area

include("/Users/saersamanipour/Desktop/dev/pkgs/SAFD/src/Centroid.jl")
using MS_Import
using Plots

# File import
pathin = "/Users/saersamanipour/Desktop/dev/ongoing/juliahrms"
filenames = ["100PPB DRUGS.mzXML"]

mz_thresh=[0,500]
int_thresh=200

ch = import_files(pathin,filenames,mz_thresh,int_thresh)

mz_vals = ch["MS1"]["Mz_values"]
mz_int = ch["MS1"]["Mz_intensity"]


res=20000
min_int=5000

mz_val_cent,mz_int_cent,dm_c = centroid(ch["MS1"]["Mz_values"],ch["MS1"]["Mz_intensity"],min_int,res); 

dm_c_p = dmz_pred_batch(mz_val_cent,mz_int_cent)

scatter(dm_c_p ./1000,label=false)
xlabel!("Scan Number")
ylabel!("Predicted peak width (mDa)")
savefig("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/Drug_100PPB.png")

CSV.write("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/Drug_100PPB_predicted.csv",
DataFrame(dm_c_p,:auto))


# 

max_numb_iter=1000
max_t_peak_w=300

min_ms_w=0.02
r_thresh=0.85

sig_inc_thresh=5
S2N=2

min_peak_w_s=3







# SAFD

max_numb_iter=1000
max_t_peak_w=400
res=20000
min_ms_w=0.02
r_thresh=0.75
min_int=2000
sig_inc_thresh=5
S2N=2
min_peak_w_s=3
method =  "MDM" # "CT" # "BG" # "RFM" #
mdm = 0.02




@time chrom1=deepcopy(ch)
mz_val = chrom1["MS1"]["Mz_values"]
mz_int = chrom1["MS1"]["Mz_intensity"]
Rt = chrom1["MS1"]["Rt" ]
FileName = filenames[1][1:end-6]
path = "/Users/saersamanipour/Desktop/dev/pkgs/SAFD/test"

rep_table,final_table = safd(mz_val,mz_int,Rt,FileName,path,max_numb_iter,max_t_peak_w,res,min_ms_w,r_thresh,
    min_int,sig_inc_thresh,S2N,min_peak_w_s)

mz_val_cent,mz_int_cent,dm_c = centroid(mz_val::Array{Float64,2},mz_int::Array{Float64,2},min_int::Int64,res::Int64)

mz_vals = deepcopy(mz_val_cent);
mz_int = deepcopy(mz_int_cent);

@time safd_s3d_cent(mz_val_cent,mz_int_cent,ch["MS1"]["Rt"],filenames[1][1:end-6],pathin,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,method,dm_c)

#@time safd_s3D(mz_vals,mz_int,Rt,FileName,pathin,max_numb_iter,max_t_peak_w,res,min_ms_w,r_thresh,
#    min_int,sig_inc_thresh,S2N,min_peak_w_s)





"""
