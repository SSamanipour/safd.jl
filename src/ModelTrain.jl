using DataFrames
using CSV
#using Plots
using BenchmarkTools
using Random
#using ScikitLearn
#using ScikitLearn.CrossValidation: cross_val_score
#using ScikitLearn.CrossValidation: train_test_split
#using PyCall
using EvoTrees


Random.seed!(11);

#@sk_import ensemble: RandomForestRegressor
#jl = pyimport("joblib")


function final_list(p)

    # i=1
    filenames = readdir(p)

    list_f = zeros(1,5)
    for i=1:size(filenames,1)

        if filenames[i][1] != "." && filenames[i][end-8:end] == "_cent.csv"
            println(i)

            list_f = vcat(list_f,hcat(feature_build(filenames[i],p),1 .* ones(size(feature_build(filenames[i],p),1),1)))
        end

    end

    table=DataFrame(list_f[2:end,:],[:ScanNum,:MeasMass,:Int,:PeakWitdh,:AqMode])

    return(list_f[2:end,:],table)
end



###########################################
# CSVs to matrices


function feature_build(file_name,p)

    # p = "/Users/saersamanipour/Desktop/dev/pkgs/SAFD/Train_data"

    list = DataFrame(CSV.File(joinpath(p,file_name)));
    feature_list = zeros(size(list));

    feature_list[:,1] = 100 .* (list.ScanNum ./ maximum(list.ScanNum));
    feature_list[:,2] = list.MeasMass;

    scan_list = unique(list.ScanNum);

    rel_int = 0

    for i =1:length(scan_list)

        sm = sum(list.Int[list.ScanNum .== scan_list[i]])
        rel_int = vcat(rel_int,list.Int[list.ScanNum .== scan_list[i]] ./ sm)

    end

    feature_list[:,3] = 100 .* rel_int[2:end];

    feature_list[:,4] = 1000 .* (list.PeakWitdh);



    return(feature_list)
end

#############################################
# R2 

function r2(y_p,y_r)
    r2 = round(var(y_p) / var(y_r),digits =3)
    return r2
end


#############################################
# Stratification 

function strat_split(par, X, test_size; random_state = random_state, limits = limits)

    n = length(par)
    bin = collect(1:n)

    for i = 1: (length(limits)-1)
        bin[limits[i].<= par].= i
    end

    X_train_, X_test_, y_train, y_test = train_test_split(collect(1:length(par)), par, test_size = test_size, random_state = random_state, stratify = bin)
    X_train = X[X_train_,:]
    X_test = X[X_test_,:]

    return  X_train, X_test, y_train, y_test
end



#############################################
# Parameters

p = "/Users/emcms/Desktop/dev/pkgs/safd.jl/Train_data"

list_f, data = final_list(p);

data = data[data[!,"PeakWitdh"] .> 1, :]

#############################################
# Splitting 

train_ratio = 0.9
train_indices = randperm(nrow(data))[1:Int(round(train_ratio * nrow(data)))]

train_data = data[train_indices, :]
eval_data = data[setdiff(1:nrow(data), train_indices), :]

x_train, y_train = Matrix(train_data[:,[1,2,3]]) , train_data[:,4]
x_eval, y_eval = Matrix(eval_data[:,[1,2,3]]), eval_data[:, 4]

#############################################
# RF EvoTrees

config = EvoTreeRegressor(
    nrounds=2000, 
    eta=0.01, 
    max_depth=9, 
    lambda=0.1, 
    rowsample=0.9, 
    colsample=1.0)



model = fit_evotree(config;
    x_train, y_train,
    x_eval, y_eval,
    metric = :mae,
    early_stopping_rounds=100,
    print_every_n=10)


# 
pred_train = model(x_train)
pred_test = model(x_eval)

r2_tr = r2(pred_train,y_train)
r2_ts = r2(pred_test,y_eval)

p1 = "/Users/emcms/Desktop/dev/pkgs/safd.jl/src"

EvoTrees.save(model, joinpath(p1,"QToF_model.bson"))
