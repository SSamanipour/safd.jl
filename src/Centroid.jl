
#module Centroid

using CSV
using DataFrames
using LsqFit
using Dierckx
using Statistics
using BenchmarkTools
#using ProgressMeter
#using SpecialFunctions
# using Plots
using ProgressBars




################################################################
# This module is to perform self-adjusting centroiding of HRMS profile data.
#
#
################################################################


####################################################################
#Simple moving average
"""
function sma(y, n)

    vals = zeros(size(y,1) ,1)
    for i in 1:size(vals,1)-(n-1)
        vals[i+1] = mean(y[i:i+(n-1)])
    end

    vals[1]=y[1]
    vals[end]=y[end]
    return vals
end
"""
################################################################
# function to find the cent mass

function find_cent(fit,ms_vec,int_vec,m,scan)


    @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))
    @.model1(x, p) =  (p[1]/pi)*(p[3]/((x-p[2])^2 + p[3]^2)) * (1+erf((1/(2*p[3]))*(x-p[2])))

    if m == 1
        y_r=model(ms_vec,fit.param)
    elseif m == 2
        y_r=model1(ms_vec,fit.param)
    end

    # plot(ms_vec,int_vec)
    # plot!(ms_vec,y_r)

    ms_cn = mean([ms_vec[argmax(y_r)],ms_vec[argmax(int_vec)]])
    int_cn = maximum(int_vec)

    #spl = Spline1D(ms_vec,int_vec)


    mz_vec = ms_vec[1]:0.0001:ms_vec[end];
    #mm = spl(mz_vec);
    if m == 1
        mm = model(mz_vec,fit.param)
    elseif m == 2
        mm = model1(mz_vec,fit.param)
    end
    # plot!(mz_vec,mm)

    ind_apx = argmax(mm);
    if isnothing(findfirst(x -> x<= int_cn/2, mm[ind_apx:end])) == 0 && 
        findfirst(x -> x<= int_cn/2, mm[ind_apx:end]) + ind_apx  <= length(mz_vec)
        
        mz_h = mz_vec[ind_apx + findfirst(x -> x<= int_cn/2, mm[ind_apx:end])]
    else
        mz_h = mz_vec[end]
    end

    if isnothing(findlast(x -> x<= int_cn/2, mm[1:ind_apx])) == 0
        mz_l = mz_vec[findlast(x -> x<= int_cn/2, mm[1:ind_apx])]
    else
        mz_l = mz_vec[1]

    end

    dm = round(mz_h - mz_l,digits=4)
    #p = plot()
    #plot!(ms_vec,int_vec,label="raw data")
    #plot!(ms_vec,sma(int_vec,3),label="smooth data")
    #plot!(ms_vec,y_r,label="modeled data")
    #plot!([ms_cn,ms_cn],[0,int_cn],label="centroided data")
    #xlabel!("Mass (Da)")
    #ylabel!("Intensity")
    # plot(mz_vec,mm)
    # size(mz_vec)

    #pathout = "/Volumes/SAER HD/Data/Temp_files/Figs_cent/" * string(scan)* "_" * string(ms_cn) * "_" * string(int_cn) * ".png"
    #savefig(pathout)



    return(ms_cn,int_cn,dm)


end

################################################################
# function to assess the fit

function fit_assess!(fit,ms_vec,int_vec,m,dm,r2,r_thresh)

    @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))
    @.model1(x, p) =  (p[1]/pi)*(p[3]/((x-p[2])^2 + p[3]^2)) * (1+erf((1/(2*p[3]))*(x-p[2])))

    #println(fit)

    if m == 1
        y_r=model(ms_vec,fit.param)
    elseif m == 2
        y_r=model1(ms_vec,fit.param)
    elseif m == 0
        return r2
    end

    if ms_vec[argmax(y_r)] - ms_vec[argmax(int_vec)] > dm/2
        r2 = 0

    end


    if argmax(y_r) ==1 || argmax(int_vec) == 1
        r2 = 0

    elseif argmax(y_r) ==length(y_r) || argmax(int_vec) == length(y_r)

        r2 = 0
    end


    return(r2)

end


################################################################
# function to fit a gaussian

function fit_gs(ms_vec,int_vec,dm,r_thresh)

    @.model1(x, p) =  (p[1]/pi)*(p[3]/((x-p[2])^2 + p[3]^2)) * (1+erf((1/(2*p[3]))*(x-p[2])))


    @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))

    ms_vec_s = ms_vec[int_vec .>= 0.3*maximum(int_vec)];
    int_vec_s = int_vec[int_vec .>= 0.3*maximum(int_vec)];
    m = 0
    # plot(ms_vec_s,int_vec_s)

    if length(ms_vec) >= 3 && length(ms_vec_s) >= 3
        p=zeros(3)
        p[1]=maximum(int_vec_s)
        p[2]=ms_vec_s[argmax(int_vec_s)]
        dm_m=(maximum(ms_vec_s)-minimum(ms_vec_s))
        p[3]=dm/2
        lbp=[0,ms_vec_s[argmax(int_vec_s)]-dm,0]
        ubp=[Inf,ms_vec_s[argmax(int_vec_s)]+dm,Inf]
        fit = []
        r2 = 0


        try
            fit = curve_fit(model ,ms_vec_s, sma(int_vec_s,3)[:], p)
            r2=1 - var(fit.resid) / var(int_vec_s)
            #println(r2)
            m = 1

        catch
            fit = []
            r2 = 0
            #println(r2)

        end
        #
        if r2 < r_thresh
            try
                fit = curve_fit(model ,ms_vec_s, sma(int_vec_s,3)[:], p,lower=lbp, upper=ubp)
                r2=1 - var(fit.resid) / var(int_vec_s)
                m = 1

            catch
                fit = []
                r2 = 0

            end

        end

        #
        if r2 < r_thresh
            try
                fit = curve_fit(model1 ,ms_vec_s, sma(int_vec_s,3)[:], p)
                r2 = 1 - var(fit.resid) / var(int_vec_s)
                m = 2
            catch
                fit = []
                r2 = 0

            end

        end


    elseif length(ms_vec) >= 3 && length(ms_vec_s) < 3
        p=zeros(3)
        p[1]=maximum(int_vec)
        p[2]=ms_vec[argmax(int_vec)]
        dm_m=(maximum(ms_vec)-minimum(ms_vec))
        p[3]=dm/2
        lbp=[0,ms_vec[argmax(int_vec)]-dm,0]
        ubp=[Inf,ms_vec[argmax(int_vec)]+dm,Inf]

        fit = []
        r2 = 0

        try
            fit = curve_fit(model ,ms_vec, sma(int_vec,3)[:], p)
            r2=1 - var(fit.resid) / var(int_vec)
            m = 1
        catch
            fit = []
            r2 = 0

        end
        #
        if r2 < r_thresh
            try
                fit = curve_fit(model ,ms_vec, sma(int_vec,3)[:], p,lower=lbp, upper=ubp)
                r2=1 - var(fit.resid) / var(int_vec)
                m = 1

                #println(r2)
            catch
                fit=[]
                r2=0

            end

        end

        #
        if r2 < r_thresh
            try
                fit = curve_fit(model1 ,ms_vec, sma(int_vec,3)[:], p)
                r2=1 - var(fit.resid) / var(int_vec)
                #println(r2)
                m = 2
            catch
                fit=[]
                r2=0

            end

        end


    else
        fit = []
        r2 = 0
        p = zeros(3)
        dm_m = 0

    end

    return(fit,r2,dm_m,m)

end






################################################################
# function to centroid

function cen_d!(mz_t,int_t,dm,ind_mx,r_thresh,min_int,s2n,scan)
    ms = mz_t[ind_mx]
    ms_vec = mz_t[findall(x -> ms + dm >= x >=ms -  dm ,mz_t)];
    int_vec = int_t[findall(x -> ms + dm >= x >=ms -  dm ,mz_t)];

    # plot(ms_vec,int_vec)
    if maximum(int_vec)/median(int_vec) >= s2n

        fit,r2,dm_p,m = fit_gs(ms_vec,int_vec,dm,r_thresh);

        try length(fit.param) > 0

            r2 = fit_assess!(fit,ms_vec,int_vec,m,dm,r2,r_thresh)
        catch 
            ms_cn = []
            int_cn = []
            dm_m = 0
            return(ms_cn,int_cn,int_t,dm_m)

        end 


       
        if r2 >= r_thresh && length(ms_vec) > 3

            ms_cn,int_cn,dm_m = find_cent(fit,ms_vec,int_vec,m,scan)

        else

            #p1 = plot()
            #plot!(ms_vec,int_vec,label="raw data")
            #plot!(ms_vec,sma(int_vec,3),label="smooth data")
            #xlabel!("Mass (Da)")
            #ylabel!("Intensity")
            #pathout = "/Volumes/SAER HD/Data/Temp_files/Figs_cent/bad/bad_" * string(scan)* "_"* string(ms) * "_" * string(int_t[ind_mx]) * ".png"
            #savefig(pathout)
            ms_cn = []
            int_cn = []
            dm_m = 0

        end

    else
        #p1 = plot()
        #plot!(ms_vec,int_vec,label="raw data")
        #xlabel!("Mass (Da)")
        #ylabel!("Intensity")
        #pathout = "/Volumes/SAER HD/Data/Temp_files/Figs_cent/bad/bad_" * string(scan)* "_" *string(ms) * "_" * string(int_t[ind_mx]) * ".png"
        #savefig(pathout)
        ms_cn = []
        int_cn = []
        dm_m = 0
    end

    # plot(ms_vec,int_vec)

    int_t[findall(x -> ms + dm >= x >=ms -  dm ,mz_t)] .= min_int/2;

    return(ms_cn,int_cn,int_t,dm_m)


end




################################################################
# peak finder

function peak_finder(mz_t,int_t,res,min_p_w,min_int,r_thresh,s2n,scan)

    mm = length(int_t[int_t .>= min_int])
    ms_cn_vec1 = zeros(mm)
    int_cn_vec1 = zeros(mm)
    dm_m_vec1 = zeros(mm);

    # i =46

    for i=1:mm
        #println(i)
        if maximum(int_t) >= min_int
            ind_mx = argmax(int_t)
            dm = mz_t[ind_mx]/res

            if dm < min_p_w
                dm =  min_p_w
            end
            ms_cn_vec,int_cn_vec,int_t,dm_m_vec = cen_d!(mz_t,int_t,dm,ind_mx,r_thresh,min_int,s2n,scan)
            if length(ms_cn_vec[ms_cn_vec .>0]) > 0
                ms_cn_vec1[i] = ms_cn_vec[ms_cn_vec .>0]
                int_cn_vec1[i] = int_cn_vec[ms_cn_vec .>0]
                dm_m_vec1[i] = dm_m_vec[dm_m_vec .>0]


            end


        else
            break

        end

    end


    return(ms_cn_vec1[ms_cn_vec1 .> 0],int_cn_vec1[int_cn_vec1 .> 0],dm_m_vec1[dm_m_vec1 .> 0])


end


################################################################
# Function for folding the chromatograms

function array2chrom(mz_val_c,mz_int_c,ss)

    mz_val_cent = zeros(length(ss),Int(maximum(ss)));
    mz_int_cent = zeros(length(ss),Int(maximum(ss)));

    for i =1:length(ss)
        #(i)
        tv1_m = float.(mz_val_c[i])
        tv1_i = mz_int_c[i]
        tv2 = findall(x -> x >0,tv1_m)

        if length(tv2) > 0
            mz_val_cent[i,1:length(tv1_m[tv1_m .>0])] = tv1_m[tv1_m .>0]
            mz_int_cent[i,1:length(tv1_m[tv1_m .>0])] = tv1_i[tv1_m .>0]

        end

    end

    return(mz_val_cent,mz_int_cent)

end

################################################################
# Function to convert the chromatograms to CSVs

# i = 320

function chrom2csv(mz_val_cent,mz_int_cent,dm_c,filename)

    tv = mz_val_cent[:];
    n = length(tv[tv .>0]);

    rep =  zeros(1,4);

    for i =1 : length(dm_c)
        println(i)
        tv1_m = mz_val_cent[i,:];
        tv2_m = tv1_m[tv1_m .> 0];

        tv1_i =  mz_int_cent[i,:];
        tv2_i = tv1_i[tv1_i .> 0];

        tv1_dm = dm_c[i,:][1]
        ttv1_dm = findall(x -> x >0,tv1_dm)
        if length(ttv1_dm)>0
            tv2_dm = tv1_dm[ttv1_dm];
        end

        if length(tv2_m)>0
            rep_t = hcat(i .* ones(length(tv2_m),1),tv2_m,tv2_i,tv2_dm)
            rep = vcat(rep,rep_t)

        end


    end

    table=DataFrame(rep[2:end,:],[:ScanNum,:MeasMass,:Int,:PeakWitdh])

    CSV.write(filename,table)
    return table

end


# i=500
################################################################
# Wrapping function

function centroid(mz_val::Array{Float32,2},mz_int::Array{Float32,2},min_int::Int64,res::Int64,r_thresh::Float64=0.8,min_p_w::Float64=0.02,s2n::Float64=1.5)

    mz_val_c = Array{Any,1}(undef,size(mz_val,1));
    mz_int_c = Array{Any,1}(undef,size(mz_val,1));
    dm_c = Array{Any,1}(undef,size(mz_val,1));
    ss = zeros(size(mz_val,1),1);
    # i=240
    for i in ProgressBar(1:size(mz_val,1))
    #@showprogress 1 "centroiding..." for i =1:size(mz_val,1)
    #sleep(0.5)
    #for i =1:500#size(mz_val,1)
    #    println(i)
        mz_t = mz_val[i,:]
        int_t = mz_int[i,:];
        scan = i
        ms_cn_vec,int_cn_vec,dm_m_vec = peak_finder(mz_t,int_t,res,min_p_w,min_int,r_thresh,s2n,scan)
        if length(ms_cn_vec) >0
            p = sortperm(ms_cn_vec)
            mz_val_c[i] = ms_cn_vec[p]
            mz_int_c[i] = int_cn_vec[p]
            dm_c[i] = dm_m_vec[p]
            ss[i] = size(ms_cn_vec,1)
        else
            mz_val_c[i] = 0
            mz_int_c[i] = 0
            dm_c[i] = 0
        end

        # plot(mz_t,int_t)
    end

    mz_val_cent,mz_int_cent = array2chrom(mz_val_c,mz_int_c,ss);
    # dm_c = Float32.(dm_c)
    mz_int_cent = Float32.(mz_int_cent)
    mz_val_cent = Float32.(mz_val_cent)
    return(mz_val_cent, mz_int_cent, dm_c)

end


# plot(dm_c)


################################################################
# End of the module
# end

################################################################
# test area

"""
# using BSON: @save, @load
# @save "mymodel.bson" model
# @load "mymodel.bson" model
using Plots 
using MS_Import

path="/Users/saersamanipour/Desktop/UvA/tempfiles"
filenames=["20180308-pos-S10D5-EFF-A.mzXML"]
mz_thresh=[0,600]
min_int = 10000
res = 50000
r_thresh = 0.8
min_p_w = 0.02
s2n = 1.5

chrom=import_files(path,filenames,mz_thresh)

mz_val = deepcopy(chrom["MS1"]["Mz_values"]);
mz_int = deepcopy(chrom["MS1"]["Mz_intensity"]);

mz_val_cent,mz_int_cent,dm_c = centroid(chrom["MS1"]["Mz_values"],chrom["MS1"]["Mz_intensity"],min_int,res); 
"""