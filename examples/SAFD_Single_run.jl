
using SAFD
using BenchmarkTools

using MS_Import

##############################################################################
# This is an example of how to process one datafile at a time.


###############################################################################
# Run the Script

# Import parameters
path="/path/to/the/file"
filenames=["filename.mzXML"]
mz_thresh=[0,380]

# Feature detection parameters
max_numb_iter=10
max_t_peak_w=300 # or 20
res=20000
min_ms_w=0.02
r_thresh=0.75
min_int=2000
sig_inc_thresh=5
S2N=2

min_peak_w_s=3


GC.gc()

 @time mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,
 polarity,Rt=import_files_MS1(path,filenames,mz_thresh)

FileName=m[1]

GC.gc()

#@time rep_table,final_table=safd(mz_vals,mz_int,t0,t_end,FileName,path,max_numb_iter,
#    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,min_peak_w_s)


@time rep_table,final_table=safd_s3D(mz_vals,mz_int,Rt,FileName,path,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s)
