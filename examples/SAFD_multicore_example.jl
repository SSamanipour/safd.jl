

using Distributed
using MS_Import
using Hwloc

addprocs((num_physical_cores()-2))

@everywhere using SAFD

pathin="C:/Users/liamo/OneDrive/jake/New folder/180419-ToFMS-JO/"

file_name = readdir(pathin)
mz_thresh=[0,0]
int_thresh=100


max_numb_iter=10


max_t_peak_w=100  #averaging parameters
res=20000 #first guess resolution
min_ms_w=0.02 #minimum ms width
r_thresh=0.8 #r value threshold
min_int=2000 #minimum intensity
sig_inc_thresh=5 #signal increase threshold first guess
S2N=2 #signal to noise ratio
min_peak_w_s=2 #minimum peak width in scans

splits_no=18 #number of splits


for i = 1:length(file_name)
    if occursin(".mzXML",file_name[i])==true
        filenames=file_name[i]
        chrom=import_files(pathin,[filenames],mz_thresh,int_thresh)
        mz_val = chrom["MS1"]["Mz_values"]
        mz_int = chrom["MS1"]["Mz_intensity" ]
        Rt = chrom["MS1"]["Rt" ]

        MZ_VAL,MZ_INT,Rt_mat,FILENAME,PATHIN,Max_it,Max_t_peak_w,Res,Min_ms_w,R_thresh,Min_int,Sig_inc_thresh,S2N1,Min_peak_w_s,C=split_s_safd(mz_val,mz_int,Rt,filenames,pathin,max_numb_iter,
        max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,splits_no)

        safdresult=Vector(undef,splits_no)
        safdresult=pmap(safd_multicore,MZ_VAL,MZ_INT,Rt_mat,FILENAME,PATHIN,Max_it,Max_t_peak_w,Res,Min_ms_w,R_thresh,Min_int,Sig_inc_thresh,S2N1,Min_peak_w_s)

        safdstitched=SAFD_stitch(safdresult,splits_no,C,pathin,filenames)
        GC.gc()
    end
end



